Icons for Unix

Other Writable/Sticky
 nf-mdi-lock_open_outline;  nf-mdi-folder_lock_open
 nf-mdi-pencil_lock

Directory
 nf-oct-file_directory

FIFO/Pipes
ﳣ nf-

Sockets
ﳧ nf-

Set{G/U}id
 nf-
 nf-

Device files
 nf-
 nf-
 nf-

Regular
 nf-oct-file


Symlinks
 nf-oct-file_symlink_file;  nf-oct-file_symlink_directory
 nf-oct-link_external;
 nf-oct-question;
---
 nf-fa-external_link;       nf-fa-external_link_square;
---
                            ﬒ nf-mdi-file_hidden
 nf-mdi-link_variant;       nf-mdi-link_variant_off
 nf-mdi-link;               nf-mdi-link_off


Binaries
 nf-oct-file_binary


Executables
 nf-oct-terminal;
---
 nf-dev-terminal_badge;     nf-dev-terminal
 nf-mdi-code_greater_than;  nf-mdi-code_greater_than_or_equal
 nf-oct-rocket

Shell Files
 nf-fa-terminal

Archives
 nf-oct-file_zip


Audio
 nf-oct-file_media
---
 nf-fa-file_audio_o;  nf-mdi-file_music


Video
 nf-oct-file_media
---
 nf-fa-file_video_o;  nf-mdi-file_video


Video and Picture
 nf-oct-file_media
---
 nf-                  nf-
