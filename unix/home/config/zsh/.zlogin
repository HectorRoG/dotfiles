#
# $ZDOTDIR/.zlogin  OR  ~/.zlogin
#

## Printing for debugging purposes if session is interactive
[[ -o interactive ]] && echo "Sourced: \$ZDOTDIR/.zlogin"

if [[ -o login ]];
then
	[[ -r "$HOME/.login" ]] && source "$HOME/.login"
fi
