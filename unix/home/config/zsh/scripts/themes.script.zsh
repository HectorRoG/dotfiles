#!/bin/zsh
#
# $ZDOTDIR/scripts/themes.script.zsh  OR  ~/.themes.script.zsh
#
# @dievilz's script for sourcing themes for Zsh.

## Printing for debugging purposes if session is interactive
[[ -o interactive ]] && echo "Sourced: \$ZDOTDIR/scripts/themes.script.zsh"


set_pwrlvl9k_zshtheme() \
{
	case "$1" in
		"omz") ZTHEME_FOUND=1
		;;
		"zinit") zinit ice wait'!' lucid depth=1
			zinit light powerlevel9k/powerlevel9k && ZTHEME_FOUND=1
		;;
		"zgen") zgen load powerlevel9k/powerlevel9k && ZTHEME_FOUND=1
		;;
		"zplug") zplug "powerlevel9k/powerlevel9k", as:theme && ZTHEME_FOUND=1
		;;
	esac

	[[ ${ZTHEME_FOUND:-0} -eq 1 ]] && ZSH_THEME=powerlevel9k
}

set_pwrlvl9k_zshtheme_custom() \
{
	[[ ${ZTHEME_FOUND:-0} -eq 0 ]] && return

	case "$1" in
		"random"|"") PWRLVL=$(jot -w %i -r 1 1 4+1) ;;
		*) PWRLVL="$1" ;;
	esac

	# echo "$PWRLVL"
	case "$PWRLVL" in
		"1") . "${ZDOTDIR:-$HOME/.config/zsh}/themes/pl9k/default.pl9k.zsh"
		;;
		"2") . "${ZDOTDIR:-$HOME/.config/zsh}/themes/pl9k/custom.pl9k.zsh"
		;;
		"3") . "${ZDOTDIR:-$HOME/.config/zsh}/themes/pl9k/dievilz.pl9k.zsh"
		;;
		"4") . "${ZDOTDIR:-$HOME/.config/zsh}/themes/pl9k/mavam.pl9k.zsh"
		;;
		*) printf "Custom config not found or doesn't exist...\n" ;;
	esac
	unset PWRLVL
}
# ==============================================================================


set_pwrlvl10k_zshtheme() \
{
	zle -lL zle-line-init > /dev/null && zle -D zle-line-init

	case "$1" in
		"omz") ZTHEME_FOUND=1
		;;
		"zinit") zinit ice wait'!' lucid atload'true; _p9k_precmd' nocd
			zinit light dievilz/powerlevel10k && ZTHEME_FOUND=1
		;;
		"zgen") zgen load dievilz/powerlevel10k && ZTHEME_FOUND=1
		;;
		"zplug") zplug "dievilz/powerlevel10k", as:theme && ZTHEME_FOUND=1
		;;
	esac

	[[ ${ZTHEME_FOUND:-0} -eq 1 ]] && ZSH_THEME=powerlevel10k
}

set_pwrlvl10k_zshtheme_custom() \
{
	[[ ${ZTHEME_FOUND:-0} -eq 0 ]] && return

	case "$1" in
		"random"|"") PWRLVL=$(jot -w %i -r 1 1 2+1) ;;
		*) PWRLVL="$1" ;;
	esac

	# echo "$PWRLVL"
	case "$PWRLVL" in
		"1") . "${ZDOTDIR:-$HOME/.config/zsh}/themes/p10k/default.p10k.zsh"
		;;
		"2") . "${ZDOTDIR:-$HOME/.config/zsh}/themes/p10k/dievilz-icons.p10k.zsh"
		;;
		"3") . "${ZDOTDIR:-$HOME/.config/zsh}/themes/p10k/dievilz.p10k.zsh"
		;;
		*) printf "Custom config not found or doesn't exist...\n" ;;
	esac
	unset PWRLVL
}
# ==============================================================================


set_spaceship_zshtheme() \
{
	case "$1" in
		"omz") ZTHEME_FOUND=1
		;;
		"zinit")
			zinit for \
				atclone" ./starship init zsh > init.zsh
				./starship completions zsh > _starship" \
				from'gh-r' nocompile sbin src'init.zsh' \
				starship/starship && ZTHEME_FOUND=1
		;;
		"zgen") zgen load spaceship/spaceship && ZTHEME_FOUND=1
		;;
		"zplug") zplug "spaceship/spaceship", as:theme && ZTHEME_FOUND=1
		;;
	esac

	[[ ${ZTHEME_FOUND:-0} -eq 1 ]] && ZSH_THEME=spaceship
}

set_spaceship_zshtheme_custom() \
{
	[[ ${ZTHEME_FOUND:-0} -eq 0 ]] && return
}
# ==============================================================================


set_geometry_zshtheme() \
{
	case "$1" in
		"omz") ZTHEME_FOUND=1
		;;
		"zinit") zinit wait'0' lucid atload'geometry::prompt' light-mode for \
			geometry/geometry && ZTHEME_FOUND=1
		;;
		"zgen") zgen load geometry/geometry && ZTHEME_FOUND=1
		;;
		"zplug") zplug "geometry/geometry", as:theme && ZTHEME_FOUND=1
		;;
	esac

	[[ ${ZTHEME_FOUND:-0} -eq 1 ]] && ZSH_THEME=geometry
}

set_geometry_zshtheme_custom() \
{
	[[ ${ZTHEME_FOUND:-0} -eq 0 ]] && return
}
# ==============================================================================


set_hyperzsh_zshtheme() \
{
	case "$1" in
		"omz") ZTHEME_FOUND=1
		;;
		"zinit") zinit ice wait depth=1; zinit light hyperzsh/hyperzsh && ZTHEME_FOUND=1
		;;
		"zgen") zgen load hyperzsh/hyperzsh && ZTHEME_FOUND=1
		;;
		"zplug") zplug "hyperzsh/hyperzsh", as:theme && ZTHEME_FOUND=1
		;;
	esac

	[[ ${ZTHEME_FOUND:-0} -eq 1 ]] && ZSH_THEME=hyperzsh
}

set_hyperzsh_zshtheme_custom() \
{
	[[ ${ZTHEME_FOUND:-0} -eq 0 ]] && return
}

################################################################################





# ------------------------------------------------------------------------------

usage_zshthemes() {
echo
echo "Zsh Theme Sourcing Script"
printf "SYNOPSIS: ./themes.script.zsh [<theme> [<plugin_manager>|<custom> [<config_number>|<random>]]][-h]\n"
printf "./themes.script.zsh [-<thm>|--<theme> [omz|zgen|zinit|zplug]] \n"
printf "./themes.script.zsh [-<thm>|--<theme> [custom [<config_number>|<random>]]] \n"
cat <<-'EOF'

OPTIONS:
   -p9,--powerlevel9k     Powerlevel9k theme by @bhilburn
      <plugin_manager>       Choose {omz,zgen,zinit or zplug} as plugin manager
      custom                  Source a custom configuration
   -p10,--powerlevel10k   Powerlevel9k theme by @romkatv
      <plugin_manager>       Choose {omz,zgen,zinit or zplug} as plugin manager
      custom                  Source a custom configuration
   -spa,--spaceship       Spaceship theme by @
      <plugin_manager>       Choose {omz,zgen,zinit or zplug} as plugin manager
      custom                  Source a custom configuration
   -geo,--geometry        Geometry theme by @
      <plugin_manager>       Choose {omz,zgen,zinit or zplug} as plugin manager
      custom                  Source a custom configuration
   -hyp,--hyperzsh        Hyperzsh theme by @
      <plugin_manager>       Choose {omz,zgen,zinit or zplug} as plugin manager
      custom                  Source a custom configuration

   -h,--help              Show this menu


   All options and option-arguments are mutually exclusive.

   If a custom configuration was selected as "random" or no one at all, the script will choose a config randomly.

EOF
}

main_zshthemes() \
{
	case $1 in
		"-h"|"--help")
			usage_zshthemes
			return 0
		;;
		"-p9"|"--powerlevel9k")
			case "$2" in
				"custom")
					set_pwrlvl9k_zshtheme_custom "$3"
				;;
				*)
					set_pwrlvl9k_zshtheme "$2"
				;;
			esac
		;;
		"-p10"|"--powerlevel10k")
			case "$2" in
				"custom")
					set_pwrlvl10k_zshtheme_custom "$3"
				;;
				*)
					set_pwrlvl10k_zshtheme "$2"
				;;
			esac
		;;
		"-spa"|"--spaceship")
			case "$2" in
				"custom")
					set_spaceship_zshtheme_custom "$3"
				;;
				*)
					set_spaceship_zshtheme "$2"
				;;
			esac
		;;
		"-geo"|"--geometry")
			case "$2" in
				"custom")
					set_geometry_zshtheme_custom "$3"
				;;
				*)
					set_geometry_zshtheme "$2"
				;;
			esac
		;;
		"-hyp"|"--hyperzsh")
			case "$2" in
				"custom")
					set_hyperzsh_zshtheme_custom "$3"
				;;
				*)
					set_hyperzsh_zshtheme "$2"
				;;
			esac
		;;
		"random")
			export ZSH_THEME=random
		;;
		*)
			usage_zshthemes
			return 127
		;;
	esac
}

## Unalias other (inside) /lib/?.zsh
unalias which-command &> /dev/null
unalias run-help &> /dev/null

main_zshthemes "$@"
