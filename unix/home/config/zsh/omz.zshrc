#############################################
## dievilz' DEFAULT ZSH RUN COMMANDS FILE  ##
#############################################

############################# SET PLUGINS & THEMES #############################

## Which plugins would you like to load?
## Add wisely, as too many plugins slow down shell startup:
case "$(uname -s)" \
in
	"Darwin") plugins=(osx)
	;;
	"Linux")
		case "$LINUX" in
			"Arch"|"ArchLinux") plugins=(archlinux) ;;
			"Void"|"VoidLinux") plugins=(voidlinux) ;;
		esac
	;;
esac

plugins+=(
	alias-finder extract git-flow-avh wd z fast-alias-tips
)
## Possible plugins: command-not-found dirhistory tiny-care-terminal
## web-search


## My custom settings for POSIX  3rd Party plugins/Custom plugins
while IFS= read -r -d '' shllplugn;
do
	[[ -r "$shllplugn" ]] && . "$shllplugn"
done \
< <(find "/opt/shell/plugins" -name "*.plugin.sh" -print0 2> /dev/null)
unset shllplugn

# ------------------------------------------------------------------------------

case "$theme" in
	"") theme=$(jot -w %i -r 1 1 6+1) ;; # NumOfResults, Min, (Max+1)
esac

case "$theme" \
in
	"1") . "$THEMES_SCRIPT" --powerlevel9k omz ;;
	"2") . "$THEMES_SCRIPT" --powerlevel10k omz ;;
	"3") . "$THEMES_SCRIPT" --spaceship omz ;;
	"4") . "$THEMES_SCRIPT" --geometry omz ;;
	"5") . "$THEMES_SCRIPT" --hyperzsh omz ;;
	*) ZSH_THEME=random ;;
esac
unset theme


################## COMPILATION OF {COMPLETION,PLUGINS,THEMES} ##################

## Sourcing my custom Configurator   OR   Oh My Zsh
case "$opt" in
	"") opt=$(jot -w %i -r 1 1 2+1) # NumOfResults, Min, (Max+1)
esac

case "$opt" \
in
	"1")
		[[ -r "$ZSH"/configure.zsh ]] && . "$ZSH"/configure.zsh
	;;
	"2")
		## Path to your "oh-my-zsh" installation.
		[[ -r "/opt/oh-my-zsh/oh-my-zsh.sh" ]] && export OMZ="/opt/oh-my-zsh"

		# Uncomment one of the following lines to change the auto-update behavior
		# zstyle ':omz:update' mode disabled  # disable automatic updates
		zstyle ':omz:update' mode auto        # update automatically without asking
		# zstyle ':omz:update' mode reminder  # just remind me to update when it's time

		# Uncomment the following line to change how often to auto-update (in days).
		zstyle ':omz:update' frequency 10

		## Sourcing Oh My Zsh ##################################################
		[[ -r "$OMZ"/oh-my-zsh.sh ]] && . "$OMZ"/oh-my-zsh.sh

		### Unalias Oh My Zsh predefined aliases
		## Inside /lib/correction.zsh
		unalias cp &> /dev/null
		unalias ebuild &> /dev/null
		unalias gist &> /dev/null
		unalias heroku &> /dev/null
		unalias hpodder &> /dev/null
		unalias man &> /dev/null
		unalias mkdir &> /dev/null
		unalias mv &> /dev/null
		unalias mysql &> /dev/null
		unalias sudo &> /dev/null
		unalias su &> /dev/null
		## Inside /lib/misc.zsh
		unalias _ &> /dev/null
		unalias afind &> /dev/null

		## Unsetopt Oh My Zsh predefined options
		unsetopt SHARE_HISTORY   ## Imports new lines from $HISTFILE, and append typed lines to $HISTFILE
	;;
esac
unset opt

## Unalias other (inside) /lib/?.zsh
unalias which-command &> /dev/null
unalias run-help &> /dev/null

########################## THEMES (CUSTOM SETTINGS) ############################

## Sourcing my custom Themes settings
[[ -n $custom ]] && . "$THEMES_SCRIPT" --"$ZSH_THEME" custom "$custom" && unset custom

############################# LAST CONFIGURATIONS ##############################

## Set keyboard layout for Non-Darwin (macOS) systems.
if [[ "$(uname -s)" != "Darwin" ]];
then
	case "$(id -un)" \
	in
		0) if command -v loadkeys > /dev/null 2>&1; then loadkeys es; fi
		;;
		*) if command -v setxkbmap > /dev/null 2>&1; then setxkbmap -layout es; fi
		;;
	esac
fi

##################### SYNTAX HIGHLIGHTING; Should be last ######################

. "$ZLE_SCRIPT" "zsh-users" "zsh-users" "zdharma" && echo

################################# END OF FILE ##################################

## Remove PATH duplicated entries
# command -v perl &> /dev/null &&
# PATH="$(perl -e 'print join(":", grep { not $seen{$_}++ } split(/:/, $ENV{PATH}))')"
