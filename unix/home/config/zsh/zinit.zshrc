#######################################
## dievilz' ZINIT RUN COMMANDS FILE  ##
#######################################


## Printing for debugging purposes if session is interactive
if \
[[ -t 1 ]] && \
[[ -o interactive ]]; then
	echo "Sourced: \$ZDOTDIR/zinit.zshrc"
else
	return   ## ...if not running interactively
fi


############################### PLUGINS & THEMES ###############################

## Path to your zinit installation.
[[ -d "/opt/zinit" ]] && export ZINIT_HOME="/opt/zinit"
if [[ ! -d "$ZINIT_HOME" ]]; then
	echo "Zinit folder not found!"
	return 1
fi

## Initial Zinit's hash definition, if configuring before loading Zinit:
declare -A ZINIT

        export ZINIT[BIN_DIR]="$ZINIT_HOME" #
       export ZINIT[HOME_DIR]="$XDG_DATA_HOME/zinit"
                  export ZPFX="${ZINIT[HOME_DIR]}/polaris"
    export ZINIT[PLUGINS_DIR]="$ZSH/plugins"
export ZINIT[COMPLETIONS_DIR]="$ZSH/completions"
   export ZINIT[SNIPPETS_DIR]="$ZSH/snippets"
 export ZINIT[ZCOMPDUMP_PATH]="$ZSH_CACHE_DIR/completions/zinit-zcompdump-${HOST}-${ZSH_VERSION}.zwc"
  export ZINIT[COMPINIT_OPTS]="-C"
  export ZINIT[MUTE_WARNINGS]=1
export ZINIT[OPTIMIZE_OUT_DISK_ACCESSES]=

### Added by Zplugin/Zinit's installer (if you source zinit.zsh after compinit)
if source "$ZINIT_HOME/zinit.zsh";
then
	# autoload -Uz _zinit
	# (( ${+_comps} )) && _comps[zinit]=_zinit
### End of Zplugin/Zinit installer's chunk

	## Load a few important annexes required without Turbo
	zinit light-mode compile'handler' for \
	  zdharma-continuum/zinit-annex-patch-dl \
	  zdharma-continuum/zinit-annex-readurl \
	  zdharma-continuum/zinit-annex-bin-gem-node \
	  zdharma-continuum/zinit-annex-submods \

	zinit lucid for \
	  if'command -v tmux > /dev/null 2>&1' \
	  atinit'
	   ZSH_TMUX_FIXTERM=false
	   ZSH_TMUX_AUTOSTART=false
	   ZSH_TMUX_AUTOCONNECT=false' \
	     OMZP::tmux \
	  \
	  wait'2' \
	     zdharma-continuum/declare-zsh \
	  \
	  wait'1' \
	     zdharma-continuum/zui \
	     psprint/zsh-navigation-tools \
	  \
	  wait'1' pick'z.sh' light-mode atinit'
	  [[ ! -d "$XDG_DATA_HOME/z/z" ]] && mkdir -p "$XDG_DATA_HOME/z"; \
	  [[ ! -r "$XDG_DATA_HOME/z/z" ]] && touch "$XDG_DATA_HOME/z/z"' \
	     knu/z \
	  \
	  wait'[[ -n "${ZLAST_COMMANDS[(r)cras*]}" ]]' \
	     zdharma-continuum/zplugin-crasis \
	  # \
	  # wait'1' bindmap"^R -> ^H" \
	  #    zdharma-continuum/history-search-multi-word \

	zinit wait lucid light-mode for \
	     OMZP::alias-finder \
	     OMZP::command-not-found \
	     OMZP::composer \
	     OMZP::extract \
	     OMZP::git-auto-fetch \
	     OMZP::git-flow-avh \
	  \
	  if'[[ $(uname -s) == "Darwin" ]]' \
	     OMZP::macos \
	  \
	     OMZP::pyenv \
	     OMZP::wd \
	     zsh-users/zsh-history-substring-search \
	     sei40kr/zsh-fast-alias-tips \
	  \
	  as'completion' \
	     OMZP::docker/_docker \
	  \
	  from'gh-r' as'program' \
	     sei40kr/fast-alias-tips-bin \
	  \
	  atinit'ZINIT[COMPINIT_OPTS]=-C; zicompinit; zicdreplay' \
	     zdharma-continuum/fast-syntax-highlighting \
	  \
	  atload'!_zsh_autosuggest_start' \
	     zsh-users/zsh-autosuggestions \
	  \
	  atpull'zinit creinstall -q .' blockf  \
	     zsh-users/zsh-completions \

	source "$ZLE_SCRIPT" custom ausg hist syhi

	## My custom settings for POSIX  3rd Party plugins/Custom plugins
	while IFS= read -r -d '' shllplugn;
	do
	    [[ -r "$shllplugn" ]] && . "$shllplugn"
	done \
	< <(find "/opt/shell/plugins" -name "*.plugin.sh" -print0 2> /dev/null)
	unset shllplugn

	# --------------------------------------------------------------------------

	case "$theme" in
	   "") theme=$(jot -w %i -r 1 1 5+1) ;; # NumOfResults, Min, (Max+1)
	esac
	case "$theme" \
	in
	   "1") . "$THEMES_SCRIPT" --powerlevel9k zinit ;;
	   "2") . "$THEMES_SCRIPT" --powerlevel10k zinit ;;
	   "3") . "$THEMES_SCRIPT" --spaceship zinit ;;
	   "4") . "$THEMES_SCRIPT" --geometry zinit ;;
	   *)   . "$THEMES_SCRIPT" --hyperzsh zinit ;;
	esac
	unset theme

	## Sourcing my custom Themes settings
	[[ -n $custom ]] && . "$THEMES_SCRIPT" --"$ZSH_THEME" custom "$custom" && \
	unset custom
fi

## Unalias other (inside) /lib/?.zsh
unalias which-command &> /dev/null
unalias run-help &> /dev/null
unalias date &> /dev/null

############################# LAST CONFIGURATIONS ##############################

## Set keyboard layout for Non-Darwin (macOS) systems.
if [[ "$(uname -s)" != "Darwin" ]];
then
	case "$(id -un)" \
	in
		0) command -v loadkeys > /dev/null 2>&1 && loadkeys es
		;;
		*) command -v setxkbmap > /dev/null 2>&1 && setxkbmap -layout es
		;;
	esac
fi

################################# END OF FILE ##################################

### Remove PATH duplicated entries
# command -v perl &> /dev/null &&
# PATH="$(perl -e 'print join(":", grep { not $seen{$_}++ } split(/:/, $ENV{PATH}))')"
