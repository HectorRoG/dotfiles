"
" ~/.config/nvim/init.vim
"

scriptencoding utf-8


" ################################# SETTINGS ###################################

let s:runconf = filereadable(expand("~/.config/vim/vimrc", 1))
if !s:runconf
	source ~/.config/vim/vimrc
else
	source ~/.vimrc
endif


" ################################# PLUGINS ####################################

let t:plugfile = filereadable(expand("~/.config/nvim/plugins.vim", 1))
if !t:plugfile
	source ~/.config/nvim/plugins.vim
else
	source ~/.nvim/plugins.vim
endif
