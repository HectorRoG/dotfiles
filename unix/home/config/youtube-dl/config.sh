#
# youtube-dl configuration file
#

#### GENERAL OPTIONS ###########################################################
## Abort downloading of further videos (in the playlist or the command line) if
## an error occurs
--abort-on-error



#### GEO RESTRICTION ###########################################################
## Bypass geographic restriction via faking X-Forwarded-For HTTP header
--geo-bypass



#### VIDEO SELECTION OPTIONS ###################################################
## Download only the video, if the URL refers to a video and a playlist.
--no-playlist



#### DOWNLOAD OPTIONS ##########################################################
## Abort downloading when some fragment is not available
--abort-on-unavailable-fragment

## Use the specified external downloader. Currently supports aria2c,avconv,axel
## ,curl,ffmpeg,httpie,wget
# --external-downloader "curl"

## Give these arguments to the external downloader
# --external-downloader "axel"
# --external-downloader-args "-n 10 -a"



#### FILESYSTEM OPTIONS ########################################################
## File containing URLs to download ('-' for stdin), one URL per line. Lines
## starting with '#', ';' or ']' are considered as comments and ignored.
# -a, --batch-file FILE

## Save all videos under Downloads directory in your home directory
--output ~/Downloads/%(title)s.%(ext)s

## Do not resume partially downloaded files (restart from beginning)
--no-continue

## Do not use .part files - write directly into output file
--no-part

## Do not use the Last-modified header to set the file modification time
--no-mtime

## Write video description to a .description file
--write-description



#### VIDEO FORMAT OPTIONS ######################################################
## Video format code, see the "FORMAT SELECTION" for all the info
## If you want to download several formats of the same video use a comma as a
## separator, e.g. -f 22,17,18 will download all these three formats, of course
## if they are available. Or a more sophisticated example combined with the
## precedence feature: -f 136/137/mp4/bestvideo,140/m4a/bestaudio.
# --format
# --format bestvideo+bestaudio/best
# --format bestvideo+bestaudio/bestvideo[ext=mp4]

## Prefer free video formats unless a specific one is requested
--prefer-free-formats

## Do not download the DASH manifests and related data on YouTube videos
--youtube-skip-dash-manifest

## If a merge is required (e.g. bestvideo+bestaudio), output to given container
## format. One of mkv, mp4, ogg, webm, flv. Ignored if no merge is required
--merge-output-format mp4



#### SUBTITLE OPTIONS ##########################################################
## Write subtitle file
--write-sub

## Write automatically generated subtitle file (YT only)
--write-auto-sub

## Languages of the subtitles to download (optional) separated by commas, use
## --list-subs for available language tags
--sub-lang eng



#### AUTHENTICATION OPTIONS ####################################################
## Login with this account ID
# -u, --username USERNAME

## Account password. If this option is left out, youtube-dl will ask interactively.
# -p, --password PASSWORD

## Two-factor authentication code
# -2, --twofactor TWOFACTOR

## Use .netrc authentication data
# -n # --netrc



#### POST-PROCESSING OPTIONS ###################################################
## Specify audio format: "best", "aac", "flac", "mp3", "m4a", "opus", "vorbis",
## or "wav"; "best" by default; No effect without -x
--audio-format best

## Specify ffmpeg/avconv audio quality, insert a value between 0 (better) and
## 9 (worse) for VBR or a specific bitrate like 128K (default 5)
--audio-quality 0

## Encode the video to another format if necessary (currently supported:
## mp4|flv|ogg|webm|mkv|avi)
--recode-video mp4

## Embed thumbnail in the audio as cover art
--embed-thumbnail

## Prefer ffmpeg over avconv for running the postprocessors (default)
--prefer-ffmpeg

## Location of the ffmpeg/avconv binary; either the path to the binary or its
## containing directory.
# --ffmpeg-location
