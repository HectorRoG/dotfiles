# Which information to display.
# NOTE: If 'ascii' will be used, it must come first.
# Default: first example below
# Valid: space separated string
#
PF_INFO="ascii title os host kernel uptime pkgs shell editor wm de memory palette"

# Separator between info name and info data.
# Default: unset
# Valid: string
PF_SEP=":"

# Enable/Disable colors in output:
# Default: 1
# Valid: 1 (enabled), 0 (disabled)
PF_COLOR=1

# Color of info names:
# Default: unset (auto)
# Valid: 0-9
PF_COL1=4

# Color of info data:
# Default: unset (auto)
# Valid: 0-9
PF_COL2=9

# Color of title data:
# Default: unset (auto)
# Valid: 0-9
PF_COL3=1

# Alignment padding.
# Default: unset (auto)
# Valid: int
PF_ALIGN="8"

# Which ascii art to use.
# Default: unset (auto)
# Valid: string
# PF_ASCII=""

# The below environment variables control more
# than just 'pfetch' and can be passed using
# 'HOSTNAME=cool_pc pfetch' to restrict their
# usage solely to 'pfetch'.

# Which user to display.
# USER=""

# Which hostname to display.
# HOSTNAME=""

# Which editor to display.
# EDITOR=""

# Which shell to display.
# SHELL=""

# Which desktop environment to display.
# XDG_CURRENT_DESKTOP=""
