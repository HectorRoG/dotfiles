#
# $XDG_CONFIG_HOME/lf/config.sh
#


# ======== ICONS ==========
## This is the list for lf icons
## From https://github.com/mohkale/dotfiles/blob/master/core/profile
refreshLfIcons() {
	if [ -r "${XDG_CONFIG_HOME:-$HOME/.config}/lf/icons" ];
	then

		## If the system already has GNU's sed, it won't be recognized
		case "$(uname -s)" in
			"Darwin"|*BSD) ## So check only if it's macOS/*BSD
				if command -v gsed > /dev/null 2>&1; then : ;
				else
					printf "GNU sed cannot be found! Aborting..."
					return
				fi
			;;
		esac

		LF_ICONS=
		LF_ICONS=$(sed "${XDG_CONFIG_HOME:-$HOME/.config}/lf/icons" \
            -e 's/[ \t]*#.*$//' \
            -e '/^[ \t]*#/d'    \
            -e '/^[ \t]*$/d'    \
            -e 's/[ \t]\+/=/g'  \
            -e 's/$/ /' 2> /dev/null)

		## Load icons from human readable config file
		LF_ICONS="$(echo "$LF_ICONS" | tr '\n' ':')"
		# LF_ICONS="${LF_ICONS//$'\n'/:}"
		export LF_ICONS
	fi
}
