#############################################
## dievilz' DEFAULT BASH RUN COMMANDS FILE ##
#############################################


## Printing for debugging purposes if session is interactive
if \
[[ -n "$PS1" ]] && \
[[ $- == *i* ]]; then
	echo "Sourced: ~/.bashrc"
else
	return   ## ...if not running interactively
fi


## If the session is login-interactive and my ~/.bash_profile
## is present, it will source this bashrc, so we need to avoid
## a sourcing loop between both files: for that we just
## simply check for a different type of session.
if ! shopt -q login_shell && [[ "$-" == *i* ]];
then
	## macOS-Only: If the session is non-login interactive,
	## any of ~/.(profile, bash_profile or bash_login) won't
	## be sourced, only bashrcs will be. We need to source ~/
	## .bash_profile because we need our env vars initialized.
	if [[ "$(uname -s)" = "Darwin" ]];
	then
		if [[ "${BPROFILE_SOURCED:-0}" -eq 0 ]];
		then
			[[ -r "$HOME/.bash_profile" ]] && source "$HOME/.bash_profile"
		fi
	fi
fi


################################### OPTIONS ####################################

shopt -s checkwinsize # Make bash check its window size after a process completes
shopt -s nocaseglob;  # Case-insensitive globbing (used in pathname expansion)
shopt -s histappend;  # Append to $HISTFILE, rather than overwriting it
shopt -s cdspell;     # Autocorrect typos in path names when using `cd`

## Enable some Bash 4 features when possible:
## * `autocd`, e.g. `**/qux` will enter `./foo/bar/baz/qux`
## * Recursive globbing, e.g. `echo **/*.txt`
for option in autocd globstar;
do
   shopt -s "$option" 2>/dev/null;
done


################################### THEMES #####################################

# ============= WINDOW TITLE ================


# =============== PROMPTS ===================
### PS1 Default interaction prompt
PS1="\[\033]0;\w\007\]" ## Set the terminal title to the current working directory
PS1+='
\w
\u@\h
\$ '

### PS2 Multiline/continuation interactive prompt
PS2="\[\033[m\]> \[\033[m\]"

### PS4 Debug mode interactive prompt
fileToDebug="$(basename "$0")"
PS4='${fileToDebug}.$LINENO+ '
unset fileToDebug


################################### PLUGINS ####################################

## My custom settings for POSIX  3rd Party plugins/Custom plugins
if [[ "${BASH_VERSINFO:-0}" -ge 4 ]];
then
	while IFS= read -r -d '' shllplugn;
	do
		[[ -r "$shllplugn" ]] && source "$shllplugn"
	done \
	< <(find "/opt/shell/plugins" -name "*.plugin.sh" -print0 2> /dev/null)
fi

[[ -f "/opt/bash/plugins/win-sudo/s/path.sh" ]] && source "/opt/bash/plugins/win-sudo/s/path.sh"


################################### THEMES #####################################

case "$theme" in
	"") theme=$(jot -w %i -r 1 1 6+1) ;; # NumOfResults, Min, (Max+1)
esac

case "$theme" in
	"1")
		[[ -r "$BASHDOTDIR/themes/dievilz-default.theme.bash" ]] \
		&& source "$BASHDOTDIR/themes/dievilz-default.theme.bash"
	;;
esac
unset theme


############################# LAST CONFIGURATIONS ##############################

## Set keyboard layout for Non-Darwin (macOS) systems.
if [[ "$(uname -s)" != "Darwin" ]];
then
	case "$(id -un)" \
	in
		0) if command -v loadkeys > /dev/null 2>&1; then loadkeys es; fi ;;
		*) if command -v setxkbmap > /dev/null 2>&1; then setxkbmap -layout es; fi ;;
	esac
fi

# php-version 7
# nvm alias default 12.14.1 >/dev/null
# # pyenv shell 3.8.1
# chruby 2.7.0

################################# COMPLETION ###################################

## Add tab completion for many Bash commands
if command -v brew >/dev/null 2>&1 && [[ -f "$(brew --prefix)/share/bash-completion/bash_completion" ]];
then
	source "$(brew --prefix)/share/bash-completion/bash_completion";

elif [[ -f "$(brew --prefix)/etc/bash_completion" ]];
then
	source "$(brew --prefix)/etc/bash_completion";

elif [[ -f /etc/bash_completion ]];
then
	source /etc/bash_completion;
fi;

## Add tab completion for SSH hostnames based on "$HOME"/.ssh/config, ignoring wildcards
[[ -r "$HOME/.ssh" ]] && complete -o "default" -o "nospace" -W "$(grep "^Host" "$HOME/.ssh" | grep -v "[?*]" | cut -d " " -f2- | tr ' ' '\n')" scp sftp ssh;

if [[ "$(uname -s)" = "Darwin" ]];
then
	## Add tab completion for `defaults read|write NSGlobalDomain`
	## You could just use `-g` instead, but I like being explicit
	complete -W "NSGlobalDomain" defaults;

	## Add `killall` tab completion for common apps
	complete -o "nospace" -W "Contacts Calendar Dock Finder Mail Safari iTunes SystemUIServer Terminal Twitter" killall;
fi

################################################################################

## Remove PATH duplicated entries
# PATH="$(perl -e 'print join(":", grep { not $seen{$_}++ } split(/:/, $ENV{PATH}))')"

## This file should be intended to work as its equivalent in Z-shell: zlogin
## But since this file, in Bash, doesn't work as that, but as the last replacement
## of a ~/.bash_profile or a ~/.profile, let's source directly a proper .login file.
if shopt -q login_shell && [[ "$-" == *i* ]];
then
	[[ -r "$HOME/.login" ]] && source "$HOME"/.login
fi

################################# END OF FILE ##################################

export BASHRC_SOURCED=1
