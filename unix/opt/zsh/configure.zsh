#
# config.zsh
#
# Configuration script for Zsh.

## Protect against non-zsh execution.
if ! ps -p $$ | egrep -m 1 -q '\b(zsh)'; then return; fi

## If OMZ is not defined, use the current script's directory.
[[ -z "$ZSH" ]] && export ZSH="${${(%):-%x}:a:h}"

## Printing for debugging purposes if session is interactive
if \
[[ -t 1 ]] && \
[[ -o interactive ]]; then
   echo "Sourced: $ZSH/configure.script.zsh"
else
   return   ## ...if not running interactively
fi

## Set ZSH_CACHE_DIR to the path where cache files should be created
## or else we will use the default /cache
[[ -z "$ZSH_CACHE_DIR" ]] && ZSH_CACHE_DIR="${XDG_CACHE_HOME:-$HOME/.cache}/zsh"

## Create cache and completions dir and add to $fpath
(( ${fpath[(Ie)"$ZSH_CACHE_DIR/completions"]} )) || fpath=("$ZSH_CACHE_DIR/completions" $fpath)



########################### INITIALIZE CONFIGURATION ###########################

## Add a function path
fpath=("$ZSH/functions" "$ZSH/completions" $fpath)

## Load all stock functions (from $fpath files) called below.
autoload -U compaudit compdef compinit zrecompile

is_plugin() {
	local base_dir="$1"
	local name="$2"
	builtin test -f "$base_dir/$name/$name.plugin.zsh" || \
	builtin test -f "$base_dir/$name/_$name" || \
	builtin test -f "$base_dir/$name.plugin.zsh" || \
	builtin test -f "$base_dir/_$name" || \
}

## Add all defined plugins to fpath. This must be done
## before running compinit.
for plugin ($plugins);
do
	if is_plugin "$ZSH/plugins/custom" "$plugin";
	then
		fpath=("$ZSH/plugins/custom/$plugin" $fpath)

	elif is_plugin "$ZSH/plugins" "$plugin";
	then
		fpath=("$ZSH/plugins/$plugin" $fpath)
	else
		echo "Plugin '$plugin' not found"
	fi
done

builtin test -f "$ZSH/themes.zsh"
fpath=("$ZSH/themes.zsh" $fpath)


################################## COMPLETION ##################################

## Save the location of the current completion dump file.
if [ -z "$ZSH_COMPDUMP" ]; then
	ZSH_COMPDUMP="${ZSH_CACHE_DIR:-$HOME/.cache/zsh}/completions/zcompdump-${HOST}-${ZSH_VERSION}.zwc"
fi

## Construct zcompdump metadata
zcompdump_revision="#revision: $(builtin cd -q "$ZSH"; git rev-parse HEAD 2>/dev/null)"
zcompdump_fpath="#fpath: $fpath"

## Delete the zcompdump file if zcompdump metadata changed
if ! command grep -q -Fx "$zcompdump_revision" "$ZSH_COMPDUMP" 2>/dev/null \
|| ! command grep -q -Fx "$zcompdump_fpath" "$ZSH_COMPDUMP" 2>/dev/null;
then
	command rm -f "$ZSH_COMPDUMP"
	zcompdump_refresh=1
fi

if [[ "$ZSH_DISABLE_COMPFIX" != true ]];
then
	## If completion insecurities exist, warn the user
	# List of the absolute paths of all unique insecure directories, split on
	# newline from compaudit()'s output resembling:
	#
	#     There are insecure directories:
	#     /usr/share/zsh/site-functions
	#     /usr/share/zsh/5.0.6/functions
	#     /usr/share/zsh
	#     /usr/share/zsh/5.0.6
	#
	# Since the ignorable first line is printed to stderr and thus not captured,
	# stderr is squelched to prevent this output from leaking to the user.
	local -aU insecure_dirs
	insecure_dirs=( ${(f@):-"$(compaudit 2>/dev/null)"} )

	# If no such directories exist, get us out of here.
	if [[ -z "${insecure_dirs}" ]] then : ; else

	# List ownership and permissions of all insecure directories.
	print "[oh-my-zsh] Insecure completion-dependent directories detected:"
	ls -ld "${(@)insecure_dirs}"

	cat <<EOD
For safety, we will not load completions from these directories until
you fix their permissions and ownership and restart zsh.
See the above list for directories with group or other writability.
To fix your permissions you can do so by disabling
the write permission of "group" and "others" and making sure that the
owner of these directories is either root or your current user.
The following command may help:
    compaudit | xargs chmod g-w,o-w
If the above didn't help or you want to skip the verification of
insecure directories you can set the variable ZSH_DISABLE_COMPFIX to
"true" before completion.script.zsh is sourced in your zshrc file.
EOD
	fi

	## compinit
	## -i: silently ignore insecure files/dirs, -u: use all files found, -C: skip security check if dumpfile exists, -d: use dumpfile,

	# ## Load only from secure directories
	compinit -i -C -d "${ZSH_COMPDUMP}"
else
	## If the user wants it, load from all found directories
	compinit -u -C -d "${ZSH_COMPDUMP}"
fi

## Append zcompdump metadata if missing
if (( $zcompdump_refresh ));
then
	## Use `tee` in case the $ZSH_COMPDUMP filename is invalid, to silence the error
	## See https://github.com/ohmyzsh/ohmyzsh/commit/dd1a7269#commitcomment-39003489
	tee -a "$ZSH_COMPDUMP" &>/dev/null <<EOF

$zcompdump_revision
$zcompdump_fpath
EOF
fi
unset zcompdump_revision zcompdump_fpath zcompdump_refresh

## zcompile the completion dump file if the .zwc is older or missing.
# zrecompile -q -p "$ZSH_COMPDUMP" && command rm -f "$ZSH_COMPDUMP.zwc.old"



################################# LOAD PLUGINS #################################

## Load all of the plugins that were defined in ~/.zshrc
for plugin ($plugins);
do
	if [ -f "$ZSH/plugins/custom/$plugin/$plugin.plugin.zsh" ]; then
		source "$ZSH/plugins/custom/$plugin/$plugin.plugin.zsh" || \
		source "$ZSH/plugins/custom/$plugin/_$plugin"
		echo "Plugin: $plugin"

	elif [ -f "$ZSH/plugins/custom/$plugin.plugin.zsh" ]; then
		source "$ZSH/plugins/custom/$plugin.plugin.zsh" || \
		source "$ZSH/plugins/custom/_$plugin"
		echo "Plugin: $plugin"

	elif [ -f "$ZSH/plugins/$plugin/$plugin.plugin.zsh" ]; then
		source "$ZSH/plugins/$plugin/$plugin.plugin.zsh" || \
		source "$ZSH/plugins/$plugin/_$plugin"
		echo "Plugin: $plugin"

	elif [ -f "$ZSH/plugins/$plugin.plugin.zsh" ]; then
		source "$ZSH/plugins/$plugin.plugin.zsh" || \
		source "$ZSH/plugins/_$plugin"
		echo "Plugin: $plugin"
	fi
done
unset plugin



################################# LOAD THEMES ##################################

source "$ZSH/themes.zsh"
theme "$ZSH_THEME"
