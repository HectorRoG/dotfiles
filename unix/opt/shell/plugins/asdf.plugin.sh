#
# /opt/shell/plugins/asdf.plugin.sh
#

## Printing for debugging purposes if session is interactive
[ -t 1 ] && \
echo "Plugin: asdf"
# echo "Sourced: /opt/shell/plugins/asdf.plugin.sh"


## Find where asdf should be installed
if [ -d /opt/asdf-vm ];
then
	[ -r "/opt/asdf-vm/asdf.sh" ] && ASDF_DIR="/opt/asdf-vm"
	[ -d "/opt/asdf-vm/completions" ] && ASDF_COMPLETIONS="$ASDF_DIR/completions"

elif [ -d /usr/local/opt/asdf-vm ];
then
	[ -r "/usr/local/opt/asdf-vm/asdf.sh" ] && ASDF_DIR="/usr/local/opt/asdf-vm"
	[ -d "/usr/local/opt/asdf-vm/completions" ] && ASDF_COMPLETIONS="$ASDF_DIR/completions"

elif [ -d "$HOME/.opt/asdf-vm" ];
then
	[ -r "$HOME/.opt/asdf-vm/asdf.sh" ] && ASDF_DIR="$HOME/.opt/asdf-vm"
	[ -d "$HOME/.opt/asdf-vm/completions" ] && ASDF_COMPLETIONS="$ASDF_DIR/completions"

elif [ -d "$HOME/.asdf" ];
then
	[ -r "$HOME/.asdf/asdf.sh" ] && ASDF_DIR="$HOME/.asdf"
	[ -d "$HOME/.asdf/completions" ] && ASDF_COMPLETIONS="$ASDF_DIR/completions"

elif [ -d "$(brew --prefix asdf > /dev/null 2>&1)" ];
then
	[ -r "$(brew --prefix asdf > /dev/null 2>&1)/asdf.sh" ] && ASDF_DIR="$(brew --prefix asdf > /dev/null 2>&1)/libexec"
	[ -d "$(brew --prefix asdf > /dev/null 2>&1)/completions" ] && ASDF_COMPLETIONS="$(brew --prefix asdf > /dev/null 2>&1)/etc/bash_completion.d"
fi


## Load command
if [ -r "$ASDF_DIR/asdf.sh" ];
then
	## Load completions
	case "$(ps -p $$ | grep -Eo -m 1 '\b\w{0,6}sh|koi')" \
	in
		"zsh")
			. "$ASDF_DIR/asdf.sh"
			[[ -o interactive ]] && fpath=(${ASDF_DIR}/completions $fpath) 2> /dev/null
		;;
		"bash")
			. "$ASDF_DIR/asdf.sh"
			[[ "$-" == *i* ]] && . "$ASDF_COMPLETIONS/asdf.bash" 2> /dev/null
		;;
	esac
fi
