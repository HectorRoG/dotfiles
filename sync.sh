#!/bin/bash
#
# Dotfiles Syncing & Management Script
# Main script for dotfiles symlinking & rsyncing
# Influenced by @mavam's and @ajmalsiddiqui's sync script
#
# By Diego Villarreal, @dievilz in GitHub (https://github.com/dievilz)

############################################# DOTFILES #############################################

syncHome_sync() \
{
	subopt "Symlinking Dotfiles to $HOME..."
	# echo $DOTFILES

	if [ ! -w "$HOME" ]; then
		echo; printf "%b" "Changing \$HOME permissions: " && chmod -v 751 "$HOME"
	fi
	echo

	while IFS= read -r -d '' dotfile;
	do
		# echo $dotfile
		case "$dotfile" in
			"$DOTFILES/unix/home/."*) continue ;;
		esac

		ln -fsv "$dotfile" "$HOME/.$(basename "$dotfile")"

		printf "%b" "Changing owner/group: " && \
		chown -hRv "$(id -un)":"$(id -gn)" "$HOME/.$(basename "$dotfile")"

		printf "%b" "Changing permissions: " && \
		chmod -v 700 "$HOME/.$(basename "$dotfile")"
	done \
	< <(find "$DOTFILES/unix/home" -maxdepth 1 -type f -print0)

	touch "$HOME"/.hushlogin

	echo; printf "%b" "Defending \$HOME: " && chmod -v 551 "$HOME"
	echo
}

syncEtc() \
{
	subopt "Symlinking Dotfiles to /etc..."

	case "$(uname -s)" in
		"MINGW"*|"MSYS"*|*"WIN"*|"Win"*|*"DOS"*) true ;;
		*) enter_sudo_mode ;;
	esac

	while IFS= read -r -d '' etcfile;
	do
		# echo $etcfile
		case "$etcfile" \
		in
			"$DOTFILES/unix/etc/."*) continue ;;
			*"/etc."*)
				# echo "$etcfile" # "$DOTFILES/unix/etc/etc.<file>"
				onlyEtcfile="$(basename "$etcfile" | \
					awk -v a="/etc." '{len=length(a)}{print substr($0,len)}')"
				# echo "$onlyEtcfile" # "<file>"
			;;
			*)
				# echo "$etcfile" # "$DOTFILES/unix/etc/<folder1>/<folder2>/<file>"
				onlyEtcfile="$(echo "$etcfile" | \
					awk -v a="$DOTFILES/unix/etc//" \
					'{len=length(a)}{print substr($0,len)}')"
				# echo "$onlyEtcfile" # "<folder1>/<folder2>/<file>"

				if [ ! -d "$(dirname "$onlyEtcfile")" ];
				then
					sudo mkdir -m 755 -pv "/etc/$(dirname "$onlyEtcfile")"
					sudo chown -v root:wheel "/etc/$(dirname "$onlyEtcfile")"
				fi
			;;
		esac
		# echo $onlyEtcfile

		[ ! -d /etc/defaults.d ] && sudo mkdir -m 700 /etc/defaults.d

		[ ! -e "/etc/defaults.d/$onlyEtcfile"_default ] && \
		sudo mv -v "/etc/$(basename $onlyEtcfile)" \
		"/etc/defaults.d/$(basename $onlyEtcfile)"_default

		sudo ln -fv "$etcfile" "/etc/$onlyEtcfile"
		sudo chown -v root:wheel "/etc/$onlyEtcfile"
	done \
	< <(fd . "$DOTFILES/unix/etc" -d 3 -t f -E "apache2" -0)
	echo

	## https://github.com/slicer69/doas#compiling-and-installing
	if [ -e /etc/pam.d/sudo ] && [ ! -e /etc/pam.d/doas ];
	then
		sudo cp -v /etc/pam.d/sudo /etc/pam.d/doas
	fi

	if [ "$(uname -s)" = "Linux" ];
	then
		if [ ! -e /etc/pam.d/sudo ] && [ ! -e /etc/pam.d/doas ];
		then
			printf "%b" "#%PAM-1.0
   @include common-auth
   @include common-account
   @include common-session-noninteractive" | sudo tee -a /etc/pam.d/doas

			sudo chown root:root /etc/pam.d/doas
		fi
	fi

	[ ! -e /etc/defaults.d/sshd_config_default ] \
	&& sudo mv -v /etc/ssh/sshd_config /etc/defaults.d/sshd_config_default
	sudo ln -fv "$DOTFILES/unix/etc/ssh/sshd_config" /etc/ssh/sshd_config

	sudo ln -fv "$DOTFILES/unix/etc/pf/pf.rules" /etc/pf/pf.rules
	echo
}
# --------------------------------------------------------------------------------------------------


syncBins() \
{
	subopt "Symlinking Dotfiles scripts to $HOME/.local/bin"
	# ln -fv "$DOTFILES"/bootstrap.sh "$HOME"/.local/bin/bootstrap
	# ln -fv "$DOTFILES"/sync.sh "$HOME"/.local/bin/sync
	echo
}

syncHomeDirs() \
{
	subopt "Making some folders on \$HOME first..."

	if [ ! -w "$HOME" ]; then
		echo; printf "%b" "Changing \$HOME permissions: " && chmod -v 751 "$HOME"
	fi
	echo
	DOTFILES="$HOME/.dotfiles"
	PUNTO_SH="$HOME/.opt/.punto-sh"
	   PUNTO="$HOME/.opt/.punto"

	[ ! -d "$DOTFILES" ] &&  mkdir -m 700 -pv "$DOTFILES"
	chmod -v 700 "$DOTFILES"
	[ ! -d "$PUNTO_SH" ] &&  mkdir -m 700 -pv "$PUNTO_SH"
	chmod -v 700 "$PUNTO_SH"
	[ ! -d "$PUNTO" ] &&  mkdir -m 700 -pv "$PUNTO"
	chmod -v 700 "$PUNTO"

	[ ! -d "$HOME/AdmonSistemas" ] && mkdir -m 700 -pv "$HOME"/AdmonSistemas
	chmod -v 700 "$HOME"/AdmonSistemas
	[ ! -d "$HOME/Desktop" ] && mkdir -m 700 -pv "$HOME"/Desktop
	chmod -v 700 "$HOME"/Desktop
	[ ! -d "$HOME/Dev" ] && mkdir -m 700 -pv "$HOME"/Dev
	chmod -v 700 "$HOME"/Dev
	[ ! -d "$HOME/Documents" ] && mkdir -m 700 -pv "$HOME"/Documents
	chmod -v 700 "$HOME"/Documents
	[ ! -d "$HOME/Downloads" ] && mkdir -m 700 -pv "$HOME"/Downloads
	chmod -v 700 "$HOME"/Downloads
	[ ! -d "$HOME/Movies" ] && mkdir -m 700 -pv "$HOME"/Movies
	chmod -v 700 "$HOME"/Movies
	[ ! -d "$HOME/Music" ] && mkdir -m 700 -pv "$HOME"/Music
	chmod -v 700 "$HOME"/Music
	[ ! -d "$HOME/Pictures" ] && mkdir -m 700 -pv "$HOME"/Pictures
	chmod -v 700 "$HOME"/Pictures
	[ ! -d "$HOME/Public" ] && mkdir -m 755 -pv "$HOME"/Public
	chmod -v 755 "$HOME"/Public
	[ ! -d "$HOME/Sites" ] && mkdir -m 755 -pv "$HOME"/Sites
	chmod -v 755 "$HOME"/Sites
	[ ! -d "$HOME/Videos" ] && mkdir -m 700 -pv "$HOME"/Videos
	chmod -v 700 "$HOME"/Videos

	[ ! -d "$HOME/AdmonSistemas/Repos" ] && mkdir -pv "$HOME"/AdmonSistemas/Repos
	[ ! -d "$HOME/Dev/Repos" ] && mkdir -pv "$HOME"/Dev/Repos
	[ ! -d "$HOME/Music/DownloadedMusic" ] && mkdir -pv "$HOME"/Music/DownloadedMusic
	[ ! -d "$HOME/Music/DownloadedSpotify" ] && mkdir -pv "$HOME"/Music/DownloadedSpotify
	[ ! -d "$HOME/Pictures/Wallpapers" ] && mkdir -pv "$HOME"/Pictures/Wallpapers

	if [ "$(uname -s)" = "Darwin" ]; then
		[ ! -d "$HOME/Dev/Xcode" ] && mkdir -pv "$HOME"/Dev/Xcode
		[ ! -d "$HOME/Library/QuickLook" ] && mkdir -pv "$HOME"/Library/QuickLook
		[ ! -d "$HOME/Music/LyricsX" ] && mkdir -pv "$HOME"/Music/LyricsX
		# [ ! -d "$HOME/Public/LyricsX" ] && mkdir -pv "$HOME"/Public/LyricsX
		# [ ! -h "$HOME/Music/LyricsX" ] && ln -fsv "$HOME"/Public/LyricsX "$HOME"/Music/LyricsX

		[ ! -d "/Library/Services" ] && sudo mkdir -pv /Library/Services
		[ ! -d "/Library/Sounds" ] && sudo mkdir -pv /Library/Sounds
		[ ! -d "/Library/Fonts" ] && sudo mkdir -pv /Library/Fonts
	fi
	echo

	#============================================================================

	subopt "Making XDG folders in \$HOME..."

	trdopt "Verifying \$XDG_CONFIG_HOME = ~/.config"
	[ ! -d "$HOME/.config" ] && mkdir -m 700 -pv "$HOME"/.config
	chmod -v 700 "$HOME"/.config
	[ ! -d "$HOME/.config/bash" ] && mkdir -pv "$HOME"/.config/bash
	chmod -v 700 "$HOME"/.config/bash
	[ ! -d "$HOME/.config/zsh" ] && mkdir -pv "$HOME"/.config/zsh
	chmod -v 700 "$HOME"/.config/zsh
	[ ! -d "$HOME/.config/shell" ] && mkdir -pv "$HOME"/.config/shell
	chmod -v 700 "$HOME"/.config/shell
	echo

	trdopt "Verifying \$XDG_CACHE_HOME = ~/.cache"
	[ ! -d "$HOME/.cache" ] && mkdir -m 700 -pv "$HOME"/.cache
	chmod -v 700 "$HOME"/.cache
	[ ! -d "$HOME/.cache/bash" ] && mkdir -pv "$HOME"/.cache/bash
	chmod -v 700 "$HOME"/.cache/bash
	[ ! -d "$HOME/.cache/zsh" ] && mkdir -pv "$HOME"/.cache/zsh
	chmod -v 700 "$HOME"/.cache/zsh
	[ ! -d "$HOME/.cache/shell" ] && mkdir -pv "$HOME"/.cache/shell
	chmod -v 700 "$HOME"/.cache/shell
	echo

	trdopt "Verifying \$XDG_DATA_HOME = ~/.local/share"
	[ ! -d "$HOME/.local" ] && mkdir -m 700 -pv "$HOME"/.local
	chmod -v 700 "$HOME"/.local
	[ ! -d "$HOME/.local/bin" ] && mkdir -m 700 -pv "$HOME"/.local/bin
	chmod -v 700 "$HOME"/.local/bin
	[ ! -d "$HOME/.local/lib" ] && mkdir -m 700 -pv "$HOME"/.local/lib
	chmod -v 700 "$HOME"/.local/lib
	[ ! -d "$HOME/.local/src" ] && mkdir -m 700 -pv "$HOME"/.local/src
	chmod -v 700 "$HOME"/.local/src
	[ ! -d "$HOME/.local/src/zsh" ] && mkdir -pv "$HOME"/.local/src/zsh
	chmod -v 700 "$HOME"/.local/src/zsh
	[ ! -d "$HOME/.local/share" ] && mkdir -m 700 -pv "$HOME"/.local/share
	chmod -v 700 "$HOME"/.local/share
	[ ! -d "$HOME/.local/share/bash" ] && mkdir -pv "$HOME"/.local/share/bash
	chmod -v 700 "$HOME"/.local/share/bash
	[ ! -d "$HOME/.local/share/zsh" ] && mkdir -pv "$HOME"/.local/share/zsh
	chmod -v 700 "$HOME"/.local/share/zsh
	[ ! -d "$HOME/.local/share/shell" ] && mkdir -pv "$HOME"/.local/share/shell
	chmod -v 700 "$HOME"/.local/share/shell
	echo

	trdopt "Verifying \$XDG_STATE_HOME = ~/.local/state"
	[ ! -d "$HOME/.state" ] && mkdir -m 700 -pv "$HOME"/.state
	chmod -v 700 "$HOME"/.state
	[ ! -d "$HOME/.local/state" ] && ln -fsv "$HOME"/.state "$HOME"/.local/state
	chmod -v 700 "$HOME"/.local/state
	[ ! -d "$HOME/.local/state/bash" ] && mkdir -pv "$HOME"/.local/state/bash
	chmod -v 700 "$HOME"/.local/state/bash
	[ ! -d "$HOME/.local/state/zsh" ] && mkdir -pv "$HOME"/.local/state/zsh
	chmod -v 700 "$HOME"/.local/state/zsh
	[ ! -d "$HOME/.local/state/shell" ] && mkdir -pv "$HOME"/.local/state/shell
	chmod -v 700 "$HOME"/.local/state/shell
	echo

	trdopt "Verifying \$XDG_OPT_HOME = ~/.opt"
	[ ! -d "$HOME/.opt" ] && mkdir -m 700 -pv "$HOME"/.opt
	chmod -v 700 "$HOME"/.opt
	echo

	#============================================================================

	magenta "Symlinking folders on \$XDG_DATA_HOME to \$HOME"

	for localsharedir in "$HOME"/.local/share/{gnupg,ssh,rage,m2,node-gyp};
	do
		[ ! -d "$localsharedir" ] && mkdir -m 700 -pv "$localsharedir"
		chmod -v 700 "$localsharedir"

		rmdir -v "$HOME/.$(basename $localsharedir)"
		[ ! -h "$HOME/.$(basename $localsharedir)" ] && ln -fsv "$localsharedir" "$HOME/.$(basename $localsharedir)"
		chmod -v 700 "$HOME/.$(basename $localsharedir)"
		echo
	done
	find $HOME/.local/share/gnupg -type f -exec chmod 600 {} \;
	find $HOME/.local/share/gnupg -type d -exec chmod 700 {} \;
	find $HOME/.local/share/ssh -type f -exec chmod 600 {} \;
	find $HOME/.local/share/ssh -type d -exec chmod 700 {} \;
	find $HOME/.local/share/rage -type f -exec chmod 600 {} \;
	find $HOME/.local/share/rage -type d -exec chmod 700 {} \;


	echo
	magenta "Symlinking folders on \$XDG_OPT_HOME to \$HOME"

	for optdir in "$HOME"/.opt/{Code,VSCodium,sqldeveloper,anydesk};
	do
		[ ! -d "$optdir" ] && mkdir -m 700 -pv "$optdir"
		chmod -v 700 "$optdir"

		rmdir -v "$HOME/.$(basename $optdir)"
		[ ! -h "$HOME/.$(basename $optdir)" ] && ln -fsv "$optdir" "$HOME/.$(basename $optdir)"
		chmod -v 700 "$HOME/.$(basename $optdir)"
		echo
	done

	rmdir -v "$HOME/.vscode"
	[ ! -h "$HOME/.vscode" ] && ln -fsv "$HOME"/.opt/Code "$HOME/.vscode"
	chmod -v 700 "$HOME"/.vscode

	rmdir -v "$HOME/.vscode-oss"
	[ ! -h "$HOME/.vscode-oss" ] && ln -fsv "$HOME"/.opt/VSCodium "$HOME/.vscode-oss"
	chmod -v 700 "$HOME"/.vscode-oss


	#============================================================================

	if [ -w "$HOME" ];
	then
		printf "%b" "Defending \$HOME: " && chmod -v 551 "$HOME"
	fi
	echo
}

syncShell_sync() \
{
	subopt "Symlinking Shell files to their ~/.config/<shell> folders..."

	if [ ! -w "$HOME" ]; then
		echo; printf "%b" "Changing \$HOME permissions: " && chmod -v 751 "$HOME"
	fi
	echo

	ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"
	BASHDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/bash"
	SHELL_CONFIG="${XDG_CONFIG_HOME:-$HOME/.config}/shell"

	[ ! -d "$ZDOTDIR" ] && mkdir -pv "$ZDOTDIR"
	[ ! -d "$BASHDOTDIR" ] && mkdir -pv "$BASHDOTDIR"
	[ ! -d "$SHELL_CONFIG" ] && mkdir -pv "$SHELL_CONFIG"
	# -----------------------------------------------------------------------------------

	while IFS= read -r -d '' zfolders;
	do
		# echo "$zfolders" # "$DOTFILES/unix/home/config/zsh/<folder1>/<folderN>/";
		zfoldersPath="$(echo "$zfolders" | \
			awk -v a="$DOTFILES/unix/home/config/zsh//" \
			'{len=length(a)}{print substr($0,len)}')"
		# echo "$zfoldersPath" # "/<folder1>/<folderN>/";

		[ ! -d "$ZDOTDIR/$zfoldersPath" ] && mkdir -pv "$ZDOTDIR/$zfoldersPath"
	done \
	< <(find "$DOTFILES/unix/home/config/zsh" -type d -print0)

	while IFS= read -r -d '' zfiles;
	do
		case "$zfiles" in
			"$DOTFILES/unix/home/config/zsh/"*/.*) continue ;;
		esac

		# echo "$zfiles" # "$DOTFILES/unix/home/config/zsh/<folder1>/<folderN>/<file>";
		zfilesPath="$(echo "$zfiles" | \
			awk -v a="$DOTFILES/unix/home/config/zsh//" \
			'{len=length(a)}{print substr($0,len)}')"
		# echo "$zfilesPath" # "/<folder1>/<folder2>/<file>";

		ln -fv "$zfiles" "$ZDOTDIR/$zfilesPath"
		printf "%b" "Changing permissions: " && chmod -v 700 "$ZDOTDIR/$zfilesPath"
	done \
	< <(find "$DOTFILES/unix/home/config/zsh" -type f -print0)
	# -----------------------------------------------------------------------------------


	while IFS= read -r -d '' bthemes;
	do
		case "$bthemes" in
			"$DOTFILES/unix/home/config/bash/themes/"*/.*) continue ;;
		esac

		[ ! -d "$BASHDOTDIR/themes" ] && mkdir -pv "$BASHDOTDIR/themes"
		ln -fv "$bthemes" "$BASHDOTDIR/themes"
	done \
	< <(find "$DOTFILES/unix/home/config/bash/themes" -type f -print0)

	while IFS= read -r -d '' brc;
	do
		pbrc="$(basename "$brc")"
		ln -fv "$brc" "$HOME/$pbrc"
		printf "%b" "Changing permissions: " && chmod -v 700 "$HOME/$pbrc"
	done \
	< <(find "$DOTFILES/unix/home/config/bash" -maxdepth 1 -name ".bash*" -type f -print0)
	# -----------------------------------------------------------------------------------


	while IFS= read -r -d '' shllfiles;
	do
		case "$shllfiles" in
			"$DOTFILES/unix/home/config/shell/"*/.*) continue ;;
		esac

		# echo "$shllfiles" # "$DOTFILES/unix/home/config/shell/<folder1>/<folderN>/<file>";
		shllfilesPath="$(echo "$shllfiles" | \
			awk -v a="$DOTFILES/unix/home/config/shell//" \
			'{len=length(a)}{print substr($0,len)}')"
		# echo "$shllfilesPath" # "/<folder1>/<folder2>/<file>";

		if [ ! -d "$SHELL_CONFIG/$(dirname "$shllfilesPath")" ];
		then
			mkdir -m 700 -pv "$SHELL_CONFIG/$(dirname "$shllfilesPath")"
		fi
		ln -fv "$shllfiles" "$SHELL_CONFIG/$shllfilesPath"
	done \
	< <(find "$DOTFILES/unix/home/config/shell" -type f -print0)
	# -----------------------------------------------------------------------------------


	if [ -w "$HOME" ]; then
		echo; printf "%b" "Defending \$HOME: " && chmod -v 551 "$HOME"
	fi
	echo
}

syncConfig_sync() \
{
	subopt "Symlinking your local config files to their ~/.config folders..."

	while IFS= read -r -d '' configFile;
	do
		case "$configFile" in
			"$DOTFILES/unix/home/config/"*/.*) continue ;;
		esac

		# echo "$configFile" # "$DOTFILES/unix/home/config/<folder1>/<folder2>/<file>"";
		case "$(uname -s)" \
		in
			"MINGW"*|"MSYS"*|*"WIN"*|"Win"*|*"DOS"*)
				configFilePath="$(echo "$configFile" |
					awk -v a="$DOTFILES/unix/home/config/e/config//" \
					'{len=length(a)}{print substr($0,len)}')"
			;;
			*)
				configFilePath="$(echo "$configFile" |
					awk -v a="$DOTFILES/unix/home/config//" \
					'{len=length(a)}{print substr($0,len)}')"
			;;
		esac
		# echo "$configFilePath" # "/<folder1>/<folder2>/<file>""

		if [ ! -d "$(dirname "$configFilePath")" ];
		then
			mkdir -m 700 -pv "$HOME/.config/$(dirname "$configFilePath")"
		fi

		[ ! -e "$HOME/.config/$configFilePath" ] && \
		ln -fv "$configFile" "$HOME/.config/$configFilePath"
	done \
	< <(find "$DOTFILES/unix/home/config" -type f -print0)
	echo
}

syncLocal() \
{
	subopt "Copying your local data/share files to their ~/.local/share folders..."

	while IFS= read -r -d '' localShareFile;
	do
		case "$localShareFile" in
			"$DOTFILES/unix/home/local/share/"*/.*) continue ;;
		esac

		# echo "$localShareFile" # "$DOTFILES/unix/home/local/share/<folder1>/<folder2>/<file>"
		case "$(uname -s)" \
		in
			"MINGW"*|"MSYS"*|*"WIN"*|"Win"*|*"DOS"*)
				localShareFilePath="$(echo "$localShareFile" | \
					awk -v a="$DOTFILES/unix/home/local/share/al/share//" \
					'{len=length(a)}{print substr($0,len)}')"
			;;
			*)
				localShareFilePath="$(echo "$localShareFile" | \
					awk -v a="$DOTFILES/unix/home/local/share//" \
					'{len=length(a)}{print substr($0,len)}')"
			;;
		esac
		# echo "$localShareFilePath" # "<folder1>/<folder2>/<file>"

		if [ ! -d "$(dirname "$localShareFilePath")" ];
		then
			mkdir -m 700 -pv "$HOME/.local/share/$(dirname "$localShareFilePath")"
		fi

		[ ! -e "$HOME/.local/share/$localShareFilePath" ] && \
		ln -fv "$localShareFile" "$HOME/.local/share/$localShareFilePath"
	done \
	< <(find "$DOTFILES/unix/home/local/share" -type f -print0)
	echo
}

syncOpt() \
{
	subopt "Copying your local custom files (3rd-party apps) to their" \
	"~/.opt folders..."

	while IFS= read -r -d '' optFile;
	do
		case "$optFile" in
			"$DOTFILES/unix/home/opt/"*/.*) continue ;;
		esac

		# echo "$optFile" # "$DOTFILES/unix/home/opt/<folder1>/<folderN>/<file>";
		case "$(uname -s)" \
		in
			"MINGW"*|"MSYS"*|*"WIN"*|"Win"*|*"DOS"*)
				optFilePath="$(echo "$opt" | \
					awk -v a="$DOTFILES/unix/home/opt/e/opt//" \
					'{len=length(a)}{print substr($0,len)}')"
			;;
			*)
				optFilePath="$(echo "$opt" | \
					awk -v a="$DOTFILES/unix/home/opt//" \
					'{len=length(a)}{print substr($0,len)}')"
			;;
		esac
		# echo "$optFilePath" # "/<folder1>/<folder2>/<file>";

		if [ ! -d "$(dirname "$optFilePath")" ];
		then
			mkdir -m 700 -pv "$HOME/.opt/$(dirname "$optFilePath")"
		fi

		[ ! -e "$HOME/.local/share/$localShareFilePath" ] && \
		ln -fv "$optFile" "$HOME/.opt/$optFilePath"
	done \
	< <(find "$DOTFILES/unix/home/opt" -type f -print0)
	echo
}

syncEtcDirs() \
{
	subopt "Copying your system-wide config files (pkg manager apps) to their" \
	"/etc/<folders>..."

	case "$(uname -s)" in
		"MINGW"*|"MSYS"*|*"WIN"*|"Win"*|*"DOS"*) true ;;
		*) enter_sudo_mode ;;
	esac

	case "$(uname -s)" \
	in
		"Darwin")
			if command -v pkgin > /dev/null 2>&1;
			then
				[ ! -d /opt/pkg/etc/defaults.d ] && mkdir -m 700 /opt/pkg/etc/defaults.d

				if [ -e /opt/pkg/bin/dnsmasq ];
				then
					[ ! -e /opt/pkg/etc/rc.d/dnsmasq ] && sudo install -C -m 0755 \
					/opt/pkg/share/examples/rc.d/dnsmasq /opt/pkg/etc/rc.d/dnsmasq

					[ ! -e /opt/pkg/etc/defaults.d/dnsmasq.conf_default ] \
					&& sudo mv -v /opt/pkg/etc/dnsmasq.conf \
					/opt/pkg/etc/defaults.d/dnsmasq.conf_default

					ln -fv "$DOTFILES/unix/etc/dnsmasq/dnsmasq.conf" \
					/opt/pkg/etc/dnsmasq.conf
				fi


				if [ -e /opt/pkg/bin/privoxy ];
				then
					[ ! -e /opt/pkg/etc/rc.d/privoxy ] && sudo install -C -m 0755 \
					/opt/pkg/share/examples/rc.d/privoxy /opt/pkg/etc/rc.d/privoxy
					while IFS= read -r -d '' privoxyFile;
					do
						[ ! -e /opt/pkg/etc/defaults.d/"$(basename "$privoxyFile")"_default ] && \
						sudo mv -v /opt/pkg/etc/"$(basename "$privoxyFile")" \
						/opt/pkg/etc/defaults.d/"$(basename "$privoxyFile")"_default

						ln -fv "$privoxyFile" \
						"/opt/pkg/etc/privoxy/$(basename "$privoxyFile")"
					done \
					< <(find "$DOTFILES/unix/etc/privoxy" -maxdepth 1 -type f -print0)
				fi


				[ ! -e /opt/pkg/etc/rc.d/dbus ] && sudo install -C -m 0755 \
				/opt/pkg/share/examples/rc.d/dbus /opt/pkg/etc/rc.d/dbus
				if ! sudo grep -Fqx "dbus=YES" /opt/pkg/etc/rc.d/dbus; then
					echo "dbus=YES" | sudo tee -a /opt/pkg/etc/rc.d/dbus
				fi

				ln -fv "$DOTFILES/unix/etc/doas.conf" /opt/pkg/etc/doas.conf
				echo
			fi

			if command -v brew > /dev/null 2>&1;
			then
				[ ! -d "$(brew --prefix)/etc/defaults.d" ] \
				&& mkdir -m 700 "$(brew --prefix)/etc/defaults.d"

				if [ -e "$(brew --prefix)/bin/dnsmasq" ];
				then
					[ ! -e "$(brew --prefix)/etc/defaults.d/dnsmasq.conf_default" ] \
					&& sudo mv -v "$(brew --prefix)/etc/dnsmasq.conf" \
					"$(brew --prefix)/etc/defaults.d/dnsmasq.conf_default"

					ln -fv "$DOTFILES/unix/etc/dnsmasq.conf" \
					"$(brew --prefix)/etc/dnsmasq.conf"
				fi

				if [ -e "$(brew --prefix)/bin/dnscrypt-proxy" ];
				then
					[ ! -e "$(brew --prefix)/etc/defaults.d/dnscrypt-proxy.toml_default" ] \
					&& sudo mv -v "$(brew --prefix)/etc/dnscrypt-proxy.toml" \
					"$(brew --prefix)/etc/defaults.d/dnscrypt-proxy.toml_default"

					ln -fv "$DOTFILES/unix/etc/dnscrypt-proxy.toml" \
					"$(brew --prefix)/etc/dnscrypt-proxy.toml"
				fi

				if [ -e "$(brew --prefix)/bin/privoxy" ];
				then
					while IFS= read -r -d '' privoxyFile;
					do
						[ ! -e "$(brew --prefix)/etc/defaults.d/$(basename "$privoxyFile")"_default ] \
						&& \
						sudo mv -v "$(brew --prefix)/etc/privoxy/$(basename "$privoxyFile")" \
						"$(brew --prefix)/etc/defaults.d/$(basename "$privoxyFile")_default"

						ln -fv "$privoxyFile" \
						"$(brew --prefix)/etc/privoxy/$(basename "$privoxyFile")"
					done \
					< <(find "$DOTFILES/unix/etc/privoxy" -maxdepth 1 -type f -print0)
				fi
			fi
			echo
		;;
	esac
}

syncOptDirs() \
{
	subopt "Copying your system-wide custom files (3rd party apps) to their" \
	"/opt/<folders>..."

	while IFS= read -r -d '' rootOptFile;
	do
		case "$rootOptFile" in
			"$DOTFILES/unix/opt/"*/.*) continue ;;
		esac

		# echo "$rootOptFile" # "$DOTFILES/unix/opt/<folder1>/<folder2>/<file>"
		case "$(uname -s)" \
		in
			"MINGW"*|"MSYS"*|*"WIN"*|"Win"*|*"DOS"*)
				rootOptFilePath="$(echo "$rootOptFile" | \
					awk -v a="$DOTFILES/unix/opt/unix/opt//" \
					'{len=length(a)}{print substr($0,len)}')"
			;;
			*)
				rootOptFilePath="$(echo "$rootOptFile" | \
					awk -v a="$DOTFILES/unix/opt//" \
					'{len=length(a)}{print substr($0,len)}')"
			;;
		esac
		# echo "$rootOptFilePath" # "<folder1>/<folder2>/<file>"

		if [ ! -d "$(dirname "$rootOptFilePath")" ];
		then
			mkdir -m 700 -pv "/opt/$(dirname "$rootOptFilePath")"
		fi

		ln -fv "$rootOptFile" "/opt/$rootOptFilePath"
	done \
	< <(fd . "$DOTFILES/unix/opt/" -d 5 --type f -0)
	echo
}

####################################################################################################




########################################### TEXT EDITORS ###########################################

textedSublimeText() \
{
	option "Copying Sublime Text custom files..."

	case "$(uname -s)" \
	in
		"Darwin")
			if [ -e "$textedbinLocation" ] && [ ! -e /usr/local/bin/subl ];
			then
				subopt "Symlinking <subl> binary to /usr/local/bin..."
				sudo ln -fsv \
				"/Applications/Sublime Text.app/Contents/SharedSupport/bin/subl" \
				/usr/local/bin/subl
				echo
			fi
			textedAppLocation="$HOME/Library/Application Support/Sublime Text/Packages/"
			[ ! -d "$textedAppLocation" ] && mkdir -pv "$textedAppLocation" && echo
		;;
		"Linux") textedAppLocation="$HOME/.config/sublime-text/Packages/" ;;
		*) return ;;
	esac

	case "$1" \
	in
		"basic")
			echo '{"bootstrapped": true, "in_process_packages": [], "installed_packages": ["Package Control"]}' \
			> "$textedAppLocation/User/Package Control.sublime-settings"
			echo
			command subl --new-window
			command subl --command "Install Package Control"
			echo

			info "Execute '--text-editors stext-full' when you " \
			"make sure you have Package Control installed in ST."
			echo
		;;
		"full")
			echo
			info "Before doing so, remember to first install Package Control, that way," \
			"every package listed inside Package Control.sublime-settings will be installed."
			subopt "Do you wish to continue? [Type anything or press Enter key to skip]"
			suboptq "" && read -r skipper

			[ -z "$skipper" ] && unset $skipper && return
			unset $skipper && echo

			case $(command subl --version | cut -d ' ' -f 4) in
			4*)
				ln -fv "$DOTFILES/unix/home/config/sublime-text/Preferences.sublime-settings" \
				"$textedAppLocation/User/Preferences.sublime-settings"
			;;
			3*)
				ln -fv "$DOTFILES/unix/home/config/sublime-text/Preferences3.sublime-settings" \
				"$textedAppLocation/User/Preferences.sublime-settings"
			;;
			esac
			echo
			subopt "Hard-linking recursively the files..."
			## a:recursive,l:symls as symls,perms,t:modtime,groups,owner,D:devices&specials
			## H:hardlinks as hardls; h:human readable; --partial: ..transfers
			## link-dest=DIR: hardlink to files in DIR when unchanged; v:verbose
			rsync -aHhv --partial --link-dest="$DOTFILES/unix/home/config/sublime-text/Packages" \
			"$DOTFILES/unix/home/config/sublime-text/Packages/" "$textedAppLocation"
		;;
	esac
	echo
}

textedSublimeMerge() \
{
	option "Copying Sublime Merge custom files..."

	case "$(uname -s)" \
	in
		"Darwin")
			if [ -e "/Applications/Sublime Merge.app/Contents/SharedSupport/bin/smerge" ] \
			&& [ ! -e /usr/local/bin/smerge ];
			then
				subopt "Symlinking smerge binary to /usr/local/bin..."
				sudo ln -fsv \
				"/Applications/Sublime Merge.app/Contents/SharedSupport/bin/smerge" \
				/usr/local/bin/smerge
				echo
			fi

			subopt "Hard-linking recursively the files..."
			textedAppLocation="$HOME/Library/Application Support/Sublime Merge/Packages"
			[ ! -d "$textedAppLocation" ] && mkdir -pv "$textedAppLocation" && echo

			## a:recursive,l:symls as symls,perms,t:modtime,groups,owner,D:devices&specials
			## H:hardlinks as hardls; h:human readable; --partial: ..transfers
			## link-dest=DIR: hardlink to files in DIR when unchanged; v:verbose
			rsync -aHhv --partial --link-dest="$DOTFILES/unix/home/config/sublime-merge/Packages" \
			"$DOTFILES/unix/home/config/sublime-merge/Packages/" "$textedAppLocation"
			echo
		;;
		"Linux") true ;;
		*) return ;;
	esac
}

textedVSCodium() \
{
	option "Copying Visual Studio Code custom files..."

	case "$(uname -s)" \
	in
		"Darwin")
			if [ -e "/Applications/VSCodium.app/Contents/Resources/app/bin/codium" ] \
			&& [ ! -e /usr/local/bin/codium ];
			then
				subopt "Symlinking codium binary to /usr/local/bin..."
				sudo ln -fsv \
				"/Applications/VSCodium.app/Contents/Resources/app/bin/codium" \
				/usr/local/bin/codium
				echo
			fi

			subopt "Hard-linking recursively the files..."
			textedAppLocation="$HOME/Library/Application Support/VSCodium/User"
			[ ! -d "$textedAppLocation" ] && mkdir -pv "$textedAppLocation" && echo

			## a:recursive,l:symls as symls,perms,t:modtime,groups,owner,D:devices&specials
			## H:hardlinks as hardls; h:human readable; --partial: ..transfers
			## link-dest=DIR: hardlink to files in DIR when unchanged; v:verbose
			rsync -aHhv --partial --link-dest="$DOTFILES/unix/home/config/VSCodium/User/" \
			"$DOTFILES/unix/home/config/VSCodium/User/" "$textedAppLocation"
			echo
		;;
		"Linux") true ;;
		*) return ;;
	esac

	subopt "Installing Visual Studio Code extensions..."
	while IFS=, read -r extension <&9;
	do
		if [ -z "$extension" ]; then continue; fi
		codium --install-extension "$extension"
	done \
	9< "$DOTFILES/unix/home/config/VSCodium/extensions.csv"
	echo
}

####################################################################################################




########################################### PREFERENCES ############################################

setup_preferences() \
{
	option "Preferences: Copying preferences files for apps (like Spotify, iTerm2, etc)..."

	case "$(uname -s)" \
	in
	"Darwin")
		subopt "Enter your Spotify Unique ID/Account ID to sync your Spotify" \
		"Desktop preferences"
		suboptq "[Type 'skip'/'none'/press 'Enter' to halt]: "
		read -r spotifyUID
		case "$spotifyUID" \
		in
			n|N|no|No|"skip"|"none"|"")
				warn "Skipping this step!"
				echo
			;;
			*)
			mkdir -pv "$HOME/Library/Application Support/Spotify/Users/$spotifyUID-user"
			trdopt "Copying Spotify preferences"
			cp -v "$DOTFILES/unix/home/config/spotify/prefs" \
			"$HOME/Library/Application Support/Spotify/Users/$spotifyUID-user/prefs"
			;;
		esac

		if [ -d "$HOME/Library/Containers/com.aone.keka/Data/Library/Preferences" ];
		then
			cp -v "$DOTFILES/unix/home/Library/Containers/lyricsx/com.aone.keka.plist" \
			"$HOME"/Library/Containers/com.aone.keka/Data/Library/Preferences/
		fi

		if [ -d "$HOME/Library/Containers/ddddxxx.LyricsX/Data/Library/Preferences" ];
		then
			cp -v "$DOTFILES/unix/home/Library/Containers/lyricsx/ddddxxx.LyricsX.plist" \
			"$HOME"/Library/Containers/ddddxxx.LyricsX/Data/Library/Preferences/
		fi

		if [ -d "$HOME/Library/Containers/com.fiplab.memoryclean2/Data/Library/Preferences/" ];
		then
			cp -v "$DOTFILES/unix/home/Library/Containers/memoryclean2/com.fiplab.memoryclean2.plist" \
			"$HOME"/Library/Containers/com.fiplab.memoryclean2/Data/Library/Preferences/
		fi

		while IFS= read -r -d '' prefFile;
		do
			# echo "$prefFile" # "$DOTFILES/unix/home/Library/Preferences/<file>"
			prefFilePath="$(echo "$prefFile" | \
				awk -v a="$DOTFILES/unix/home/Library/Preferences//" \
				'{len=length(a)}{print substr($0,len)}')"
			# echo "$prefFilePath" # "/<file>"

			cp -Rv "$prefFile" "$HOME/Library/Preferences/$prefFilePath"
		done \
		< <(fd . "$DOTFILES/unix/home/Library/Preferences" -d 1 --type f -0)
		echo
	;;
	"Linux")
		warn "Nothing to add yet..."
	;;
	esac
	echo
}

############################################# LIBRARY ##############################################

setup_library_macos() \
{
	option "Library: Copying Library files for apps (like LyricsX, TimeOut, etc)..."
	isMacos

	subopt "Setting up ${IT}Renamer6${NIT} data (renamerlets)"
	subopt "Do you wish to continue? [Type anything or press Enter key to skip]"
	suboptq "" && read -r skipper
	if [ -n "$skipper" ]; then
		[ ! -d "$HOME/Library/Application Support/Renamer" ] \
		&& mkdir -pv "$HOME/Library/Application Support/Renamer"

		## a:recursive,l:symls as symls,perms,t:modtime,groups,owner,D:devices&specials
		## H:hardlinks as hardls; h:human readable; --partial: ..transfers; v:verbose;
		rsync -aHhv --partial \
		"$DOTFILES/unix/home/Library/Application Support/Renamer/Renamerlets.renamerlets" \
		"$HOME/Library/Application Support/Renamer/Renamerlets.renamerlets"
		echo
	fi
	[ -z "$skipper" ] && echo
	unset $skipper

	subopt "Setting up ${IT}Time Out${NIT} data (settings and breaks)"
	subopt "Do you wish to continue? [Type anything or press Enter key to skip]"
	suboptq "" && read -r skipper
	[ -n "$skipper" ] && setup_library_timeout
	[ -z "$skipper" ] && echo
	unset $skipper

	subopt "Setting up ${IT}yEd Editor${NIT} data (settings and bundles)"
	subopt "Do you wish to continue? [Type anything or press Enter key to skip]"
	suboptq "" && read -r skipper
	[ -n "$skipper" ] && setup_library_yed
	[ -z "$skipper" ] && echo
	unset $skipper

	subopt "Setting up ${IT}QuickLook${NIT} generators"
	subopt "Do you wish to continue? [Type anything or press Enter key to skip]"
	suboptq "" && read -r skipper
	[ -n "$skipper" ] && setup_library_quicklook
	[ -z "$skipper" ] && echo
	unset $skipper

	subopt "Setting up ${IT}Automator${NIT} Services and Quick Actions"
	subopt "Do you wish to continue? [Type anything or press Enter key to skip]"
	suboptq "" && read -r skipper
	[ -n "$skipper" ] && setup_library_automator
	[ -z "$skipper" ] && echo
	unset $skipper

	subopt "Setting up ${IT}Sounds${NIT} for System Preferences"
	subopt "Do you wish to continue? [Type anything or press Enter key to skip]"
	suboptq "" && read -r skipper
	[ -n "$skipper" ] && setup_library_sounds
	[ -z "$skipper" ] && echo
	unset $skipper
}

setup_library_timeout() \
{
	while IFS= read -r -d '' timeOutFile;
	do
		# echo "$timeOutFile" # "$DOTFILES/unix/home/Library/Group Containers/timeout/<folder1>/<file>"
		timeOutFilePath="$(echo "$timeOutFile" | \
			awk -v a="$DOTFILES/unix/home/Library/Group Containers/timeout//" \
			'{len=length(a)}{print substr($0,len)}')"
		# echo "$timeOutFilePath" # "<folder1>/<file>"

		timeOutFolder="$(find "$HOME/Library/Group Containers" \
			-name "*dejal.timeout.free" -type d -print0 -quit | \
			awk -F "Containers/" '{print $2}' | tr -d '\0')"
		# echo "$timeOutFolder" # "xxxxxxxxxx.com.dejal.timeout.free"

		if [ ! -d "$HOME/Library/Group Containers/$timeOutFolder/$(dirname "$timeOutFilePath")" ];
		then
			mkdir -pv "$HOME/Library/Group Containers/$timeOutFolder/$(dirname "$timeOutFilePath")"
		fi

		if [ ! -e "$HOME/Library/Group Containers/$timeOutFolder/$timeOutFilePath" ];
		then
			cp -Rv "$timeOutFile" \
			"$HOME/Library/Group Containers/$timeOutFolder/$timeOutFilePath"
		fi
	done \
	< <(fd . "$DOTFILES/unix/home/Library/Group Containers/timeout" -d 5 -t f -0)
	echo
}

setup_library_yed() \
{
	while IFS= read -r -d '' yEdFile;
	do
		# echo "$yEdFile" # "$DOTFILES/unix/home/Library/yWorks/yEd/<folder1>/<file>"
		yEdFilePath="$(echo "$yEdFile" | \
			awk -v a="$DOTFILES/unix/home/Library/yWorks//" \
			'{len=length(a)}{print substr($0,len)}')"
		# echo "$yEdFilePath" # "yEd/<folder1>/<file>"

		if [ ! -d "$(dirname "$yEdFilePath")" ];
		then
			mkdir -pv "$HOME/Library/yWorks/$(dirname "$yEdFilePath")"
		fi

		if [ ! -e "$HOME/Library/yWorks/$yEdFilePath" ];
		then
			cp -Rv "$yEdFile" "$HOME/Library/yWorks/$yEdFilePath"
		fi
	done < <(fd . "$DOTFILES/unix/home/Library/yWorks/yEd" -d 5 -t f -0)
	echo
}

setup_library_quicklook() \
{
	trdopt "Copying QuickLook Generators from iCloud Drive..."

	[ ! -d "$HOME/Library/QuickLook" ] && mkdir -pv "$HOME"/Library/QuickLook
	while IFS= read -r -d '' qlgen;
	do
		[ ! -d "$HOME/Library/QuickLook/$(basename $qlgen)" ] && \
		## a:recursive,l:symls as symls,perms,t:modtime,groups,owner,D:devices&specials
		## H:hardlinks as hardls; h:human readable; --partial: ..transfers; v:verbose
		rsync -aHhv --partial --exclude=".*" "$qlgen" "$HOME/Library/QuickLook/"
	done \
	< <(find "$HOME/Library/Mobile Documents/com~apple~CloudDocs/QuickLook" \
		-maxdepth 1 -name "*.qlgenerator" -type d -print0)
	echo

	trdopt "Copying QuickLook Generators config files..."
	[ -d "$HOME/Library/QuickLook/QLColorCode.qlgenerator/Contents" ] && \
	cp -v "$DOTFILES/unix/home/Library/QuickLook/QLColorCode.ql/Info.plist" \
	"$HOME"/Library/QuickLook/QLColorCode.qlgenerator/Contents/Info.plist

	[ -d "$HOME/Library/QuickLook/QLStephen.qlgenerator/Contents" ] && \
	cp -v "$DOTFILES/unix/home/Library/QuickLook/QLStephen.ql/Info.plist" \
	"$HOME"/Library/QuickLook/QLStephen.qlgenerator/Contents/Info.plist

	echo
}

setup_library_automator() \
{
	trdopt "Copying the custom QuickActions and Services from iCloud Drive..."

	[ ! -d "/Library/Services" ] && sudo mkdir -pv /Library/Services
	while IFS=$'\n' read -r -d '' workf;
	do
		# echo "$workf" # "$HOME/Library/Mobile Documents/com~apple~Automator/Documents/<dir>"
		workfPath="$(echo "$workf" | \
			awk -v a="$HOME/Library/Mobile Documents/com~apple~Automator/Documents//" \
			'{len=length(a)}{print substr($0,len)}')"
		# echo "$workfPath" # "<folder>"

		if [ ! -e "/Library/Services/$workfPath" ];
		then
			sudo cp -Rv "$workf" "/Library/Services/$workfPath"
			sudo chown -Rv root:staff "/Library/Services/$workfPath"
		fi
	done \
	< <(find "$HOME/Library/Mobile Documents/com~apple~Automator/Documents" \
		-maxdepth 1 -name "*.workflow" -type d -print0)
	echo
}

setup_library_sounds() \
{
	trdopt "Copying Sounds audio files from iCloud Drive..."

	[ ! -d "/Library/Sounds" ] && sudo mkdir -pv /Library/Sounds
	while IFS= read -r -d '' snds;
	do
		case "$snds" in
			*".icloud") continue ;;
			*"._"*) continue ;;
		esac

		if [ ! -e "/Library/Sounds/$(basename "$snds")" ];
		then
			sudo cp -v "$snds" "/Library/Sounds/"
			sudo chown -Rv root:staff "/Library/Sounds/$(basename "$snds")"
		fi
	done \
	< <(find "$HOME/Library/Mobile Documents/com~apple~QuickTimePlayerX/Documents/Alerts" \
		-maxdepth 1 -type f -print0)
	echo
}

####################################################################################################




#################################### WALLPAPERS, LYRICS & FONTS ####################################

setup_wallpapers() \
{
	option "Wallpapers: R-Syncing your favorite wallpapers"

	info "If your wallpapers are on an external drive, halt this command," \
	"plug the drive in, open a new terminal and rerun this script"
	warn "If you want to halt this setup, just type Enter to exit"

	local firstDir
	local secondDir

	echo; subopt "Write the Full Path of the Directory (without final"
	suboptq "trailing slash) you will sync the wallpapers from: "
	read -r firstDir

	[ -z "$firstDir" ] && warn "Skipping this step!" && return

	echo; subopt "Write the Full Path of the Directory (without final"
	suboptq "trailing slash) where the wallpapers will go: "
	read -r secondDir

	[ -z "$secondDir" ] && warn "R-Syncing wallpapers halted" && return

	## a:recursive,l:symls as symls,perms,t:modtime,groups,owner,D:devices&specials
	## H:hardlinks as hardls; h:human readable; P:--progress & --partial
	## v:verbose; z:compress during transfer
	echo; rsync -aHhPvz --exclude=".*" "$firstDir/" $secondDir

	echo; success "Wallpapers successfully rsynced!"
	echo
}

setup_lyrics() \
{
	option "Lyrics: R-Syncing your Lyrics files (.lrc) folder content"

	targetLyricsDir=""
	case "$(uname -s)" \
	in
		"Darwin") targetLyricsDir="/Users/Shared/LyricsX" ;;
		"Linux") targetLyricsDir="/usr/share/lyrics/" ;;
		*)
			error "$(uname -s) not supported! Skipping..."
			echo; return 126
		;;
	esac

	sourceLyricsDir=""

	info "If your lyrics are on an external drive, halt this command, plug the" \
	"drive in, open a new terminal and rerun this script"
	warn "If you want to halt this setup, just type Enter to exit"
	echo
	subopt "Write the Full Path of the Directory (without final"
	subopt "trailing slash) you will sync the lyrics from: "
	read -r lyricsDir

	[ -z "$sourceLyricsDir" ] && warn "R-Syncing lyrics halted" && return

	if [ -d "$sourceLyricsDir" ];
	then
		## a:recursive,l:symls as symls,perms,t:modtime,groups,owner,D:devices&specials
		## H:hardlinks as hardls; h:human readable; P:--progress & --partial
		## v:verbose; z:compress during transfer
		echo; rsync -aHhPvz --exclude=".*" "$sourceLyricsDir/" "$targetLyricsDir"
	else
		error "Source directory not found! Aborting..."
		echo; exit 126
	fi

	echo; success "Lyrics successfully rsynced!"
	echo
}

setup_fonts() \
{
	option "Fonts: Syncing your .ttf's, .otf's and zip files"

	local localDir
	case "$(uname -s)" \
	in
		"Darwin") localDir="$HOME/Library/Fonts/" ;;
		"Linux") localDir="/usr/share/fonts/" ;;
		*)
			error "$(uname -s) not supported! Skipping..."
			echo; return 126
		;;
	esac

	info "If your fonts are on an external drive, halt this command, plug the" \
	"drive in, open a new terminal and rerun this script"
	warn "If you want to halt this setup, just type Enter to exit"
	echo
	subopt "Write the Full Path of the Directory (without final"
	suboptq "trailing slash) you will copy the fonts from: "
	read -r fontsDir

	if [ -z "$fontsDir" ];
	then
		warn "Copying fonts halted"
		echo; exit 126
	fi

	if [ -d "$fontsDir" ];
	then
		echo
		# while IFS= read -r -d '' file;
		# do
		# 	cp -v "$file" "$localDir"
		# done \
		# < <(find "$fontsDir" -name "*.ttf" -type f -print0)
		# echo

		## a:recursive,l:symls as symls,perms,t:modtime,groups,owner,D:devices&specials
		## H:hardlinks as hardls; h:human readable; P:--progress & --partial
		## v:verbose; z:compress during transfer
		rsync -aHhPvz --include="*.ttf" "$fontsDir/" "$localDir"

		# while IFS= read -r -d '' file;
		# do
		# 	cp -v "$file" "$localDir"
		# 	chmod -v 644 "$file"
		# done \
		# < <(find "$fontsDir" -name "*.otf" -type f -print0)
		# echo

		## a:recursive,l:symls as symls,perms,t:modtime,groups,owner,D:devices&specials
		## H:hardlinks as hardls; h:human readable; P:--progress & --partial
		## v:verbose; z:compress during transfer
		rsync -aHhPvz --include="*.otf" "$fontsDir/" "$localDir"

		while IFS= read -r -d '' file;
		do
			cp -v "$file" "$localDir"
			chmod -v 644 "$file"

			if command -v minizip > /dev/null 2>&1;
			then
				minizip extract "$localDir/$file"
				rm "$localDir/$file"

			elif command -v minizip > /dev/null 2>&1;
			then
				unzip "$localDir/$file"
				rm "$localDir/$file"
			fi
		done \
		< <(find "$fontsDir" -name "*.zip" -type f -print0)
		echo

		success "Fonts successfully copied!"
	else
		error "Directory not found! Aborting..."
		echo; exit 126
	fi
	echo
}

####################################################################################################




######################################### REMOVE DOTFILES ##########################################

removeFiles() \
{
	subopt "Unlinking at $HOME"
	verifyTrashUtility

	while IFS= read -r -d '' file;
	do
		case "$(uname -s)" \
		in
			"Darwin")
				trash -v "$file"
			;;
			*)
				trash -v "$file"
			;;
		esac
	done \
	< <(fd . "$HOME" -d 1 -t l -0)
	echo;
}

removeBins() \
{
	subopt "Unlinking at $HOME/.local/bin"
	verifyTrashUtility

	while IFS= read -r -d '' binary;
	do
		case "$(uname -s)" \
		in
			"Darwin")
				trash -v "$binary"
			;;
			*)
				trash -v "$binary"
			;;
		esac
	done \
	< <(fd . "$HOME"/.local/bin -t l -0)
	echo
}

removeFolders() \
{
	subopt "Unlinking at $HOME/.config and $HOME/.local"
	verifyTrashUtility

	while IFS= read -r -d '' configFolder;
	do
		case "$(uname -s)" \
		in
			"Darwin")
				trash -v "$configFolder"
			;;
			*)
				trash -v "$configFolder"
			;;
		esac
	done \
	< <(fd . "$HOME"/.config -t l -0)

	while IFS= read -r -d '' localFolder;
	do
		case "$(uname -s)" \
		in
			"Darwin")
				trash -v "$localFolder"
			;;
			*)
				trash -v "$localFolder"
			;;
		esac
	done \
	< <(fd . "$HOME"/.local -t l -0)
	echo
}

removeTextEditors() \
{
	option "Text Editors: Unlinking existing settings"
	verifyTrashUtility

	case "$(uname -s)" \
	in
		"Darwin")
			appSTLocation="$HOME/Library/Application Support/Sublime Text/Packages/"
			appSMLocation="$HOME/Library/Application Support/Sublime Merge/Packages/"
			appVSCLocation="$HOME/Library/Application Support/VSCodium/User"
		;;
		"Linux")
			appSTLocation="$HOME/.config/sublime-text/Packages/"
			appSMLocation="$HOME/.config/sublime-merge/Packages/"
			appVSCLocation="$HOME/.config/VSCodium/User"
		;;
	esac

	while IFS= read -r -d '' file;
	do
		trash -v "$file"
	done \
	< <(find "$appSTLocation" -maxdepth 3 -type l -print0)

	while IFS= read -r -d '' file;
	do
		trash -v "$file"
	done \
	< <(find "$appSMLocation" -type l -print0)

	while IFS= read -r -d '' file;
	do
		trash -v "$file"
	done \
	< <(find "$appVSCLocation" -maxdepth 3 -type l -print0)

	echo
}

####################################################################################################




######################################## VERIFY FUNCTIONS ##########################################

verifyRealpathUtility() \
{
	if ! command -v realpath > /dev/null 2>&1;
	then
		echo; printf "%b" "\n\033[38;31mError: <realpath> command not found! Aborting...\n"
		echo; return
	fi
}

verifyRsyncUtility() \
{
	if ! command -v rsync > /dev/null 2>&1;
	then
		error "<rsync> command not found! Aborting..."
		echo; return
	fi
}

verifyFdUtility() \
{
	if ! command -v fd > /dev/null 2>&1;
	then
		error "<fd> command not found! Aborting..."
		echo; return
	fi
}

verifyTrashUtility() \
{
	if command -v trash > /dev/null 2>&1;
	then : ;
	elif command -v macos-trash > /dev/null 2>&1;
	then : ;
	else
		error "<trash> command not found! Aborting..."
		echo; return
	fi
}

####################################################################################################



# ==============================================================================

usage_sync() \
{
echo
echo "@dievilz' Dotfiles Syncing & Management Script"
echo "Main script for dotfiles symlinking & rsyncing."
echo "Influenced by @mavam's and @ajmalsiddiqui's sync script"
echo
printf "SYNOPSIS: ./%s [-a][-b][-d][-f][-h][-l][-p][-r][-t][-w] \n" "$(basename "$0")"
printf "./%s [--dotfiles [home|etcroot|bins|...]] \n" "$(basename "$0")"
printf "./%s [--text-editors [basics [stext|smerge|...]|full [stext|...]]] \n" "$(basename "$0")"
printf "./%s [--remove][files|bins|folders|text-editors] \n" "$(basename "$0")"
cat <<-'EOF'

OPTIONS:
    -d,--dotfiles       Sync the Dotfiles to $HOME and /{etc,opt} with:
       home                Symlink dotfiles to $HOME
       etc                 Syncronize system-wide config files to /etc
       bins                Copy binaries to $XDG_BIN_HOME
       homedirs            Makedir common User folders and enforce XDG compliance
       shell               Symlink Shell init files to $XDG_CONFIG_HOME
       config              Symlink custom files to $XDG_CONFIG_HOME
       local               Syncronize custom files to $XDG_DATA_HOME
       opt                 Syncronize custom files to $HOME/.opt
       etcdirs             Syncronize <PkgManager> Apps' config files to their /etc/<folder>
       optdirs             Syncronize 3rd party files to a /opt/<folder>

    -t,--text-editors   Configure Text Editors/Dev Apps settings with:
       stext-basic         Open ST to install Package Control by script
       stext-full          Once Package Control is installed, sync the rest of the files
       smerge              Sync Sublime Merge full-featured files for a complete dev usage
       vscodium            Sync VSCodium full-featured files for a complete dev usage

    -p,--preferences    Copy preferences files for apps (like Spotify, iTerm2, etc)
    -b,--library        Rsync macOS Library files (Preferences, Services, Containers, etc).
    -w,--wallpapers     Rsync Wallpapers from any external drive/local folder
    -l,--lyrics         Rsync Lyrics from any external drive/local folder
    -f,--fonts          Rsync any Fonts files from any external drive/local folder

    -r,--remove         Delete all the symlinks deployed by this script, or:
       files               Remove only symlinked files in $HOME
       bins                Remove only symlinked dotfiles binaries in $XDG_BIN_HOME
       folders             Remove only symlinked files in $XDG_CONFIG_HOME
       text-editors        Remove only symlinked text editors settings

    -h,--help           Show this menu

Note: Execute '--text-editors full <app>' when you have finished the Fresh Install
Bootstrapping, and you have synced all your plugins on the text editors.

For full documentation, see: https://github.com/dievilz/punto.sh#readme

EOF
}

menu_sync() \
{
	case $1 in
		"-h"|"--help")
			usage_sync
			exit 0
		;;
		"-d"|"--dotfiles")
			case $2 in
				"home")
					echo; syncHome_sync
				;;
				"etc")
					echo; syncEtc
				;;
				"bins")
					echo; syncBins
				;;
				"homedirs")
					echo; syncHomeDirs
				;;
				"shell")
					echo; syncShell_sync
				;;
				"config")
					echo; syncConfig_sync
				;;
				"local")
					echo; syncLocal
				;;
				"opt")
					echo; syncOpt
				;;
				"etcdirs")
					echo; syncEtcDirs
				;;
				"optdirs")
					echo; syncOptDirs
				;;
				*)
					error "Unknown option-argument for --dotfiles! Use -h/--help"
					return 127
				;;
			esac
		;;
		"-t"|"--text-editors")
			case $2 in
				"stext-basics")
					echo; textedSublimeText basic
				;;
				"smerge")
					echo; textedSublimeMerge
				;;
				"vscodium")
					echo; textedVSCodium
				;;
				"stext-full")
					echo; textedSublimeText full
				;;
				*)
					error "Unknown option-argument for --text-editors! Use -h/--help"
					return 127
				;;
			esac
		;;
		"-p"|"--preferences")
			echo; setup_preferences
		;;
		"-b"|"--library")
			echo; setup_library_macos
		;;
		"-w"|"--wallpapers")
			echo; setup_wallpapers
		;;
		"-l"|"--lyrics")
			echo; setup_lyrics
		;;
		"-f"|"--fonts")
			echo; setup_fonts
		;;
		"-r"|"--remove")
			case $2 in
				"files")
					echo; removeFiles
				;;
				"bins")
					echo; removeBins
				;;
				"folders")
					echo; removeFolders
				;;
				"text-editors")
					# echo; removeTextEditors
				;;
				""|"all")
					echo; red "3, 2, 1... Dropping the nukes! "

					echo
					removeFiles
					removeBins
					removeFolders
					# removeTextEditors

					success "Mission accomplished! Bravo Six, going bark..."
					echo
				;;
				*)
					error "Unknown option-argument for --remove! Use -h/--help"
					return 127
				;;
			esac
		;;
		*)
			error "Unknown option! Use -h/--help"
			return 127
		;;
	esac
}

main_case_sync() \
{
	option "We are going to install a concept one by one."
	until false;
	do
		echo
		subopt "Below are the options:"
		usage_sync

		subopt "Choose one option from the usage..."
		subopt "[${BO}'skip'${NBO}|${BO}'none'${NBO}][press ${BO}Enter key${NBO}] to halt"
		suboptq "" && read -r syncOpt

		case "$syncOpt" in
			"none"|"None"|"skip"|"Skip"|"")
				warn "Skipping this step!"
				return 1
				break
			;;
			*) menu_sync $syncOpt
			;;
		esac
	done
}

main_sync() \
{
	if [ "$#" -eq 0 ];
	then
		main_case_sync
	else
		menu_sync "$@"
	fi
}

verifyRealpathUtility

### Exporting DOTFILES path and source helper functions so the script can work
DOTFILES="$(realpath "$0" | grep -Eo '^.*?dotfiles')" ## -o: only matching

if [ -e "$HOME/.helpers" ];
then
	. "$HOME/.helpers" && success "Helper script found!"
	echo
else
	printf "%b" "\n\033[38;31mError: helper functions not found! Aborting...\033[0m\n"
	echo; exit 1
fi

## Verifying utilities
verifyRsyncUtility
verifyFdUtility
verifyTrashUtility

main_sync "$@"; exit
